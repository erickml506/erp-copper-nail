<?php
require( 'ssp.class.php' );

class Conexion{ 
	static function getData($table, $primaryKey, $id_client = null){
		$sql_details = array(
			'user' => 'root',
			'pass' => '',
			'db'   => 'main',
			'host' => $_ENV["DB_HOST"]
		);
		if($table == 'clients'){
			$columns = array(
				array( 'db' => 'id', 'dt' => 'id' ),
				array( 'db' => 'type', 'dt' => 'type' ),
				array( 'db' => 'document',  'dt' => 'document' ),
				array( 'db' => 'business_name',   'dt' => 'business_name' ),
				array( 'db' => 'fiscal_address',     'dt' => 'fiscal_address' ),
				array( 'db' => 'install_address',     'dt' => 'install_address' ),
				array( 'db' => 'install_hours',     'dt' => 'install_hours' ),
				array( 'db' => 'number_vehicles',     'dt' => 'number_vehicles' ),
				array( 'db' => 'user',     'dt' => 'user' ),
				array( 'db' => 'password',     'dt' => 'password' ),
				array( 'db' => 'price_gps',     'dt' => 'price_gps' ),
				array( 'db' => 'price_plataform',     'dt' => 'price_plataform' ),
				array( 'db' => 'is_rented',     'dt' => 'is_rented' ),
				array( 'db' => 'rent_expiration',     'dt' => 'rent_expiration' ),
				array( 'db' => 'purchase_order',     'dt' => 'purchase_order' ),
				array( 'db' => 'payment_method',     'dt' => 'payment_method' ),
				array( 'db' => 'operation_number',     'dt' => 'operation_number' ),
				array( 'db' => 'service_type',     'dt' => 'service_type' ),
				array( 'db' => 'status',     'dt' => 'status' )
			);
			$data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );
		}else if($table == 'users'){
			$columns = array(
				array( 'db' => 'id', 'dt' => 'id' ),
				array( 'db' => 'name', 'dt' => 'name' ),
				array( 'db' => 'apellidos', 'dt' => 'apellidos' ),
				array( 'db' => 'email', 'dt' => 'email' ),
				array( 'db' => 'type', 'dt' => 'type' ),
				array( 'db' => 'api_token', 'dt' => 'api_token' ),
				array( 'db' => 'password', 'dt' => 'password' ),
			);
			$data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );
		}else if($table == 'vehicles'){
			$columns = array(
				array( 'db' => 'id', 'dt' => 'id' ),
				array( 'db' => 'client_id', 'dt' => 'client_id' ),
				array( 'db' => 'imei', 'dt' => 'imei' ),
				array( 'db' => 'name', 'dt' => 'name' ),
				array( 'db' => 'brand', 'dt' => 'brand' ),
				array( 'db' => 'model', 'dt' => 'model' ),
				array( 'db' => 'plate', 'dt' => 'plate' ),
				array( 'db' => 'group', 'dt' => 'group' ),
				array( 'db' => 'phone', 'dt' => 'phone' ),
				array( 'db' => 'simcard', 'dt' => 'simcard' ),
				array( 'db' => 'protocol', 'dt' => 'protocol' )
			);
			$whereAll = "client_id =".$id_client;
			$data = SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll );

		}else if($table == 'contacts'){
			$columns = array(
				array( 'db' => 'id', 'dt' => 'id' ),
				array( 'db' => 'id_client', 'dt' => 'id_client' ),
				array( 'db' => 'name', 'dt' => 'name' ),
				array( 'db' => 'phone', 'dt' => 'phone' ),
				array( 'db' => 'relationship', 'dt' => 'relationship' ),
				array( 'db' => 'email', 'dt' => 'email' ),
				array( 'db' => 'type', 'dt' => 'type' )
			);
			$whereAll = "id_client =".$id_client;
			$data = SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll );

		}else if($table == 'agreements'){
			$columns = array(
				array( 'db' => 'id', 'dt' => 'id' ),
				array( 'db' => 'client_id', 'dt' => 'client_id' ),
				array( 'db' => 'vehicle_group', 'dt' => 'vehicle_group' ),
				array( 'db' => 'start_date', 'dt' => 'start_date' ),
				array( 'db' => 'due_date', 'dt' => 'due_date' )
			);
            if($id_client == null){
                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );
            }else{
                $whereAll = "client_id =".$id_client;
			    $data = SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll );
            }	
		}else if($table == 'v_charging'){
			$columns = array(
				array( 'db' => 'id', 'dt' => 'id' ),
				array( 'db' => 'client_id', 'dt' => 'client_id' ),
				array( 'db' => 'billing_date', 'dt' => 'billing_date' ),
				array( 'db' => 'amount', 'dt' => 'amount' )	
			);
			$data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );
		}
		return $data;
		
	}
	
}