<?php  
require 'flight/Flight.php';
require "Illuminate/Support/helpers.php";
require "Psr/Container/ContainerInterface.php";
spl_autoload_register(function ($className) {
        $ds = DIRECTORY_SEPARATOR;
        $dir = __DIR__;
        $className = str_replace('\\', $ds, $className);
        $file = "{$dir}{$ds}{$className}.php";
        if (is_readable($file)) {
        	require_once $file;
        }
});
require 'misc/common.php';
require 'misc/Carbon.php';
require "lib.php";
require "include-database.php";
require "data.php";
