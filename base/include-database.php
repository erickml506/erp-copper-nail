<?php 
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Container\Container;

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
            'driver'    => "mysql",
            'host'      => $_ENV["DB_HOST"],
            'database'  => "main",
            'username'  => "root",
            'password'  => "",
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

$capsule->bootEloquent();