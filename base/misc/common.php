<?php 

$phpInputData = [];

$url_clients = 'http://proxy/v1/client/';
$url_vehicles = 'http://proxy/v1/vehicles/';

function getValueFromArray($key,$array){

    return is_array($array) && array_key_exists($key,$array) ? ( is_string($array[$key]) ? \trim($array[$key]) : $array[$key]) : "";

}

function getPhpInputData(){
    global $phpInputData; 
    if(count($phpInputData)==0){ 
        $file_contents = @file_get_contents('php://input');
        if(is_string($file_contents)){ 
            $phpInputData = json_decode($file_contents,true);
        } 
    } 
    return $phpInputData;//retorna una copia y no una referencia global.
}

function getMethodArray(){
    return count($_GET)>1 ? $_GET : ( count($_POST)>1 ? $_POST : getPhpInputData() ); 
}