BASE=$(pwd)
cd base && docker build -t custom_php .
docker pull nginx
docker pull mysql:5.7
cd ..
docker pull registry.gitlab.com/erik.beltran.e/api-microgateway
#cd gateway && docker build -t geo_gateway .
cd $BASE
cd ui && docker  build -t geo_middleman .
# cd ui && bundle install