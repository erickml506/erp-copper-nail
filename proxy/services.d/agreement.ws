####### SERVICE: agreement
	location /v1/agreement {
		try_files $uri /v1/agreement/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass agreement.ws:9000;
		}
	}