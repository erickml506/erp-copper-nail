####### SERVICE: billing
	location /v1/billing {
		try_files $uri /v1/billing/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass billing.ws:9000;
		}
	}