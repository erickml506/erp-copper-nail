####### SERVICE: client
	location /v1/client {
		try_files $uri /v1/client/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass client.ws:9000;
		}
	}