####### SERVICE: session
	location /v1/session {
		try_files $uri /v1/session/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass session.ws:9000;
		}
	}