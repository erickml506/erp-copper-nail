####### SERVICE: user
	location /v1/user {
		try_files $uri /v1/user/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass user.ws:9000;
		}
	}