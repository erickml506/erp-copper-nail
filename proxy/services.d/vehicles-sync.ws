####### SERVICE: vehicles-sync
	location /v1/vehicles-sync {
		try_files $uri /v1/vehicles-sync/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass vehicles-sync.ws:9000;
		}
	}