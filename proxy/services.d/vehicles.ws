####### SERVICE: vehicles
	location /v1/vehicles {
		try_files $uri /v1/vehicles/index.php$uri;
		location ~ [^/]\.php(/|$) {
		    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
		    fastcgi_pass vehicles.ws:9000;
		}
	}