docker network create localnet
sleep 1
docker-compose -f gateway/docker-compose.yml up -d
docker-compose -f services/session/docker-compose.yml up -d
docker-compose -f services/user/docker-compose.yml up -d
docker-compose -f services/client/docker-compose.yml up -d
docker-compose -f services/billing/docker-compose.yml up -d
docker-compose -f proxy/docker-compose.yml up -d
cd ui && middleman 