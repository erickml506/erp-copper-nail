<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}

/*
|--------------------------------------------------------------------------
| ROUTES CRUD
|--------------------------------------------------------------------------
|
| Here is where the app listen all request and make actions
| CREATE, READ, UPDATE, DELETE
| 
|--------------------------------------------------------------------------
| GET ALL agrements
|--------------------------------------------------------------------------
*/
Flight::route('GET /all', function(){
    $agrements = AgreementController::getAllAgrements();
    Flight::json($agrements, 200);
 });


/*
|--------------------------------------------------------------------------
| CREATE agrements
|--------------------------------------------------------------------------
*/
Flight::route('POST /create', function()  {
    $request = Flight::request();
    $agrement = AgreementController::createAgrement($request);
    Flight::json($agrement, 201);
});


/*
|--------------------------------------------------------------------------
| UPDATE agrements
|--------------------------------------------------------------------------
*/
Flight::route('POST /update', function() {
    $request = Flight::request();
    $agrement = AgreementController::UpdateAgrement($request, $request->data->id);
    if($agrement == 'error'){
        Flight::json(['error' => "No content"], 204);
    }else{
        Flight::json($agrement, 200);
    }
});

/*
|--------------------------------------------------------------------------
| DETELE agrements
|--------------------------------------------------------------------------
*/
Flight::route('POST /delete', function()  {
    $request = Flight::request();
    $agrement = AgreementController::deleteAgrement($request->data->id);
    if($agrement == 'error'){
        Flight::json(['error' => "No content"], 204);
    }else{
        Flight::json($agrement, 200);
    }
});

/*
|--------------------------------------------------------------------------
| GET agrements BY CLIENT
|--------------------------------------------------------------------------
*/
Flight::route('POST /getAgreementsByClient', function(){
    $request = Flight::request();
    $agrements = AgreementController::getAgrementsByClient($request->data->id_client);
    Flight::json($agrements, 200);
});