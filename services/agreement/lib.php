<?php
use \Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Agreement extends Model {
    
    protected $table = 'agreements';
    protected $fillable = [
        'client_id',
        'vehicle_group', 
        'start_date', 
        'due_date'
    ];
}
class AgreementController {

    public static function getAllAgrements(){
        $agrements = Conexion::getData('agreements','id');
        return $agrements;
    }
    
    public static function requestData($request, $action = null){
        $data = [
            'client_id'=> $request->data->client_id,
            'vehicle_group'=> $request->data->vehicle_group,
            'start_date'=> $request->data->start_date,
            'due_date'=> $request->data->due_date
        ];
        return $data;
    }

    public static function createAgrement($request){

        $agrement = Agreement::create(self::requestData($request));
        return $agrement;
    }

    public static function UpdateAgrement($request, $id){
        $agrement = Agreement::find($id);
        if($agrement){
            $data = self::requestData($request);
            Agreement::where('id', $id)->update($data);
            $agrement_updated = Agreement::find($id); 
            return $agrement_updated;
        }else{
            return 'error';
        }
    }

    public static function deleteAgrement($id){
        $agrement = Agreement::find($id);
        if($agrement){
            $agrement->delete();
            return $agrement;
        }else{
            return 'error';
        }         
    }

    public static function getAgrementsByClient($id_client){
        $agrement = Conexion::getData('agreements','id', $id_client);
        return  $agrement;
    }

}


?>