<?php  
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}
/*
|--------------------------------------------------------------------------
| ROUTES CRUD
|--------------------------------------------------------------------------
|
| Here is where the app listen all request and make actions
| CREATE, READ, UPDATE, DELETE
| 
|--------------------------------------------------------------------------
| GETTING BILLINGS  FOR CHARGING
|--------------------------------------------------------------------------
*/
Flight::route('GET /getBillingToCharging', function(){
    $charging =  BillingController::getBillingToChanrging();
    Flight::json($charging, 200);
    
});
/*
|--------------------------------------------------------------------------
| GET ALL BILLING
|--------------------------------------------------------------------------
*/ 

Flight::route('GET|POST /all(/@page:[0-9]{1,9}(/@client_id:[0-9]{1,9}))', function($page=1,$client_id = null){ 
        

    $_method = getMethodArray();
    $request = Flight::request(); 
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    } 

    if(!is_numeric($page) || $page<1){  
        $page = $request->data->page;
        if(!is_numeric($page) || $page<1)$page=1;
    }else{
        $page=1;
    } 
    if(is_numeric($client_id)){
        $reg = BillingController::getAllByClient($client_id,$page,$request);
    }else{
        $reg = &BillingController::getAll((int)$page,$request);
    }
    
    Flight::json($reg, 200);
 });

Flight::route('GET /getBillingsByClient(/@id:[0-9]{1,9})', function($id){
    $_method = getMethodArray(); 
    if(!is_numeric($id))$id = getValueFromArray("id",$_method);
    $regs = &BillingController::getBillingsByClient($id);
    Flight::json($regs, 200);
 });

Flight::route('GET /getItemsByBill(/@id:[0-9]{1,9})', function($id){
    $_method = getMethodArray(); 
    if(!is_numeric($id))$id = getValueFromArray("id",$_method);
    $regs = &BillingItemController::getItemsByBill($id);
    Flight::json($regs, 200);
 });


Flight::route('GET /getAllByBill(/@id:[0-9]{1,9}(/@page:[0-9]{1,9}))', function($id,$page){
    $_method = getMethodArray();
    $request = Flight::request();
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    } 
    if(!is_numeric($id))$id = $request->data->id;
    if(!is_numeric($page))$page = $request->data->page;
    $regs = &BillingItemController::getAllByBill($id,$page,$request);
    Flight::json($regs, 200);
 });


Flight::route('GET /count(/@client_id:[0-9]{1,9})', function($client_id){ 
    $_method = getMethodArray(); 
    if(!isset($client_id)){ 
        $client_id = getValueFromArray("client_id",$_method);
    }  
    Flight::json(BillingController::count($client_id), 200);
});

/*
|--------------------------------------------------------------------------
| GET ONE BILLING
|--------------------------------------------------------------------------
*/

Flight::route('GET /(@id)', function($id)  {
    $_method = getMethodArray(); 
    if(!isset($id)){
        $id = getValueFromArray("id",$_method);
    }  
    $reg = BillingController::get($id);
    if($reg){
        Flight::json($reg, 200);
    }else{
        Flight::json(['error' => "No content"], 204);
    }
    
});

/*
|--------------------------------------------------------------------------
| CREATE BILLING
|--------------------------------------------------------------------------
*/
Flight::route('POST /create', function()  {
    $_method = getMethodArray();
    $request = Flight::request();
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    }
    $reg = BillingController::create($request);
 	Flight::json($reg, 201);
});


/*
|--------------------------------------------------------------------------
| UPDATE BILLING
|--------------------------------------------------------------------------
*/
Flight::route('PUT|POST /update(/@id)', function($id) {  
    $_method = getMethodArray();
    if(!is_numeric($id)){
        $id = getValueFromArray("id",$_method);
    }  
    if(array_key_exists("session",$_method))unset($_method["session"]);  
    if(array_key_exists("password",$_method))unset($_method["password"]);   
    if(array_key_exists("id",$_method))unset($_method["id"]);   
    
    $request = Flight::request();
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    }else{
        if($request->data->session)unset($request->data->session);
        if($request->data->password)unset($request->data->password);
        if($request->data->id)unset($request->data->id);
    } 
    $reg = BillingController::update($request, $id);
    if($reg == 'error'){
        Flight::json([
            'error' => "No content",
            "_method"=>$_method
        ], 204);
    }else{
        Flight::json($reg, 200);
    }
});

/*
|--------------------------------------------------------------------------
| DETELE BILLING
|--------------------------------------------------------------------------
*/
Flight::route('DELETE|POST /delete(/@id)', function($id=null)  {  
    $_method = getMethodArray();
    if(!is_numeric($id)){
        $id = getValueFromArray("id",$_method);
    }   
    $reg = BillingController::delete($id);
    if($reg == 'error'){
        Flight::json([
            'error' => "Not found", 
        ], 204);
    }else{
        Flight::json($reg, 200);
    }
});
/*
|--------------------------------------------------------------------------
| DETELE BILLING ITEM
|--------------------------------------------------------------------------
*/
Flight::route('DELETE|POST /deleteItem(/@id)', function($id=null)  {  
    $_method = getMethodArray();
    if(!is_numeric($id)){
        $id = getValueFromArray("id",$_method);
    }   
    $reg = &BillingItemController::delete($id);
    if($reg == 'error'){
        Flight::json([
            'error' => "Not found", 
        ], 204);
    }else{
        Flight::json($reg, 200);
    }
});  
/*
|--------------------------------------------------------------------------
| ADD BILLING ITEM
|--------------------------------------------------------------------------
*/
Flight::route('PUT|POST /addItem', function()  {  
    $request = Flight::request();
    $reg = &BillingItemController::add($request);
    Flight::json($reg, 201);
});  

Flight::route('GET|POST /allItems', function(){
    $request = Flight::request(); 
    $regs = &BillingItemController::allItemsByBill($request);
    Flight::json($regs, 200);
 });

