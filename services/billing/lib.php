<?php
require "php-curl-class-master/src/Curl/ArrayUtil.php";
require "php-curl-class-master/src/Curl/CaseInsensitiveArray.php";
require "php-curl-class-master/src/Curl/Curl.php";
require "php-curl-class-master/src/Curl/Decoder.php";
require "php-curl-class-master/src/Curl/MultiCurl.php";
require "php-curl-class-master/src/Curl/StrUtil.php";
require "php-curl-class-master/src/Curl/Url.php";

use \Illuminate\Database\Eloquent\Model;
use \Curl\Curl;

class Billing extends Model {

    const TYPE_TICKET = 0;
    const TYPE_BILL = 1;
    const TYPE_RECEIPT = 2;

    const TYPE_TEXTS = [
        self::TYPE_TICKET =>"Boleta",
        self::TYPE_BILL =>"Factura",
        self::TYPE_RECEIPT =>"Recibo",
    ];

    const CYCLE_ONLY_DEVICE = 0;
    const CYCLE_RECONNECTION_OTHERS = 20;
    const CYCLE_MONT = 1;
    const CYCLE_TRI = 3;
    const CYCLE_SEM = 6;
    const CYCLE_ANN = 12;

    const CYCLE_TEXTS = [
        self::CYCLE_ONLY_DEVICE =>"Sólo Dispositivo", 
        self::CYCLE_RECONNECTION_OTHERS =>"Reconexión u otros", 
        self::CYCLE_MONT =>"Mensual", 
        self::CYCLE_TRI =>"Trimestral", 
        self::CYCLE_SEM =>"Semestral", 
        self::CYCLE_ANN =>"Anual", 
    ];

    const DOC_TYPE_TICKET = 0;
    const DOC_TYPE_BILL = 1; 

    const DOC_TYPE_TEXTS = [
        self::DOC_TYPE_TICKET =>"Consolidada", 
        self::DOC_TYPE_BILL =>"Disgregada",  
    ];
    
    protected $table = 'billing';
    protected $fillable = [
        'client_id', 
        'code', 
        'billing_date',  
        'doc_type',  
        'type',  
        'cycle',  
    ];
    protected $hidden = [  
    ];

}

class BillingItem extends Model {
    
    protected $table = 'billing_items';
    protected $fillable = [
        'price', 
        'billing_id', 
        'vehicles_id',  
    ];
    protected $hidden = [  
    ];

}

class BillingController {

    const REGS_X_PAGE = 20;
    
    public static function getBillingToChanrging(){
        $charging = Conexion::getData('v_charging','id');
        return $charging;
    }

    public static function &applyGlobalSearch(&$builder,$search){  
        $fields = [
            "code",
            "billing_date",
        ];
        foreach ($fields as $key => $value) {
            if($value=="billing_date"){
                if(preg_match("/([\d]{4})\/([\d]{2})/",$search,$arr)===1){
                    $search_tmp = $arr[1]."-".$arr[2]."-01";
                    $builder->orWhere($value,'like',"%$search_tmp%");
                }else{
                    $builder->orWhere($value,'like',"%$search%");
                } 
            }else{
                $builder->orWhere($value,'like',"%$search%");
            }
        } 
        foreach (Billing::TYPE_TEXTS as $key => $value) {
            if(stripos($value,$search)!==false){
                $builder->orWhere("type",'=',$key);
            }
        }
        foreach (Billing::DOC_TYPE_TEXTS as $key => $value) {
            if(stripos($value,$search)!==false){
                $builder->orWhere("doc_type",'=',$key);
            }
        }
        foreach (Billing::CYCLE_TEXTS as $key => $value) {
            if(stripos($value,$search)!==false){
                $builder->orWhere("cycle",'=',$key);
            }
        }
        return $builder;
    }

    public static function &getAll($page,$request){   
        global $url_clients;
        global $url_vehicles;
        if($page>0){ 
            $global_search  = $request->data->global_search;
            $filter_field   = $request->data->filter_field;
            $filter_order   = $request->data->filter_order;//1 asc, other desc
            $builder = Billing::offset( ( $page - 1 ) * self::REGS_X_PAGE )->limit( self::REGS_X_PAGE );
            if($global_search<>""){
                $builder = &BillingController::applyGlobalSearch($builder,$global_search);
            }
            if($filter_order!=0){
                $builder->orderBy($filter_field,$filter_order==1 ? "ASC":"DESC");
            }
            //return $builder->toSql(); 

            $data = $builder->get();
            $total_reg = BillingController::count( null , false , $global_search);
            $total_pages = floor( $total_reg / ( float ) self::REGS_X_PAGE ) + ( $total_reg % self::REGS_X_PAGE > 0  ?  1 : 0);
             

            $items = [];
            foreach ($data as $key => &$value) {
                $arr_tmp = &BillingItemController::getItemsByBill($value["id"]);
                $items[$value["id"]] = $arr_tmp["data"];
            }
            
            $clients = [];
            
            $curl = new Curl();
            foreach ($data as $key => &$value) {
                $client_id = $value["client_id"];
                if(array_key_exists($client_id,$clients))continue;
                $url = $url_clients.$client_id;
                $response = $curl->get($url);  
                if(!$curl->error){
                    $clients[$client_id] = $curl->response;
                }
            }
            
            $vehicles = []; 
            $check_class = [];
            $check_data = [];

            $curl = new Curl();
            foreach ($items as $key => &$item) { 
                if($item){  
                    array_push($check_class,get_class($item));
                    foreach ($item as $item_data) { 
                        $check_data[] = $item_data;
                        $vehicle_id = $item_data->vehicles_id;
                        if(array_key_exists($vehicle_id,$vehicles))continue; 
                        $url = $url_vehicles.$vehicle_id;
                        $response = $curl->get($url);   
                        if(!$curl->error){
                            $vehicles[$vehicle_id] = $curl->response;
                        }else{
                            $vehicles[$vehicle_id] = $curl->errorCode . ': ' . $curl->errorMessage;;
                        }
                    }
                }
            }

            $res = [  
                "data" => $data , 
                "clients" => $clients , 
                "items" => $items , 
                "vehicles" => $vehicles ,   
                "check_class" => $check_class , 
                "check_data" => $check_data , 
                "details"=>[
                    "regs_x_page" => self::REGS_X_PAGE,
                    "total_pages" => $total_pages , 
                    "total_regs" => $total_reg,
                    "current_page" => $page, 
                ], 
            ];
        }else{
            $res = [];
        }
        return $res;
    }
    public static function getAllByClient($client_id,$page){
        if($client_id>0){
            $global_search = getValueFromArray("global_search",$_GET);
            $filter_field   = getValueFromArray("filter_field",$_GET);
            $filter_order   = getValueFromArray("filter_order",$_GET);//1 asc, other desc
            if($page>0){
                $builder = Billing::where( "client_id", $client_id )->offset( ( $page - 1 ) * self::REGS_X_PAGE )->limit( self::REGS_X_PAGE );
            }else{
                $builder = Billing::where( "client_id", $client_id );
            }
            if($global_search<>""){
                $builder = &BillingController::applyGlobalSearch($builder,$global_search);
            }
            if($filter_order!=0){
                $builder->orderBy($filter_field,$filter_order==1 ? "ASC":"DESC");
            }
            $data = $builder->get();
            if($page>0){ 
                $total_reg = BillingController::count( $client_id , false , $global_search );
                $total_pages = floor( $total_reg / ( float ) self::REGS_X_PAGE ) + ( $total_reg % self::REGS_X_PAGE > 0  ?  1 : 0);
                $res = [ 
                    "data" => $data , 
                    "details"=>[
                        "regs_x_page" => self::REGS_X_PAGE,
                        "total_pages" => $total_pages , 
                        "total_regs" => $total_reg,
                        "current_page" => $page,
                    ], 
                ];
            }else{
                $res = [ 
                    "data" => $data , 
                    "details"=>[ 
                    ], 
                ];
            }
        }else{
            $res = [];
        }
        return $res;
    }
    public static function &getBillingsByClient($client_id){
        $regs = Billing::where("client_id",$client_id)->get();
        $regs = ["data"=>$regs,"client_id"=>$client_id];
        return $regs;
    }
    public static function count($client_id,$return_arr = true,$global_search = ""){ 
        if($client_id){
            $builder = Billing::where( "client_id" , $client_id );
            if($global_search<>""){
                $builder = &BillingController::applyGlobalSearch($builder,$global_search);
            }
            $total = $builder->count(); 
        }else{
            $builder = Billing::query();
            if($global_search<>""){
                $builder = &BillingController::applyGlobalSearch($builder,$global_search);
            }
            $total = $builder->count(); 
        } 
        if( $return_arr ) return [ "count" => $total ]; 
        return $total;
    }
    
    public static function get($id){
        $reg = Billing::find($id);
        return $reg;
    }  
    public static function create(&$request){
         
        $updated_at = date("Y-m-d H:i:s");
        $reg = array(); 

        //-----------------------------------------------------
        $tmp = $request->data->billing_date;
        $tmp = explode("/",$tmp);
        if(is_array($tmp) && count($tmp)>1){
            //$tmp = $tmp[0]."-".$tmp[1]."-01";//2018-04-01
            $tmp = $tmp[2]."-".$tmp[1]."-".$tmp[0];//yyyy-mm-dd
            $billing_date = $tmp;
        }
        //-------------------------------------------------------
        $reg["client_id"]       = is_numeric($request->data->client_id) ? $request->data->client_id : $request->data->new_client_id;
        $reg["code"]            = $request->data->code;
        $reg["billing_date"]    = $tmp; 
        $reg["cycle"]           = $request->data->cycle; 
        $reg["doc_type"]        = $request->data->doc_type; 
        $reg["type"]            = $request->data->type; 
        $reg['created_at']      = date("Y-m-d H:i:s");
        $reg['updated_at']      = date("Y-m-d H:i:s"); 
        $reg = Billing::create($reg);
        $reg->save(); 
        return $reg;  
    }

    public static function update($request, $id){   
        $_data = $request->data->getData(); 
        $updated_at = date("Y-m-d H:i:s");
        $reg = Billing::find($id);
        $prevent = ["billing_date","new_client_id"];
        if($reg){
            foreach ($_data as $key => $value){
                if(in_array($key,$prevent))continue;
                if($key && strlen($key)>0)$reg[$key]  = $value;
            }
            $tmp = $request->data->billing_date;
            $tmp = explode("/",$tmp);
            if(is_array($tmp) && count($tmp)>1){
                //$tmp = $tmp[0]."-".$tmp[1]."-01";//2018-04-01
                $tmp = $tmp[2]."-".$tmp[1]."-".$tmp[0];//yyyy-mm-dd
                $billing_date = $tmp;
            }
            if($request->data->new_client_id && is_numeric($request->data->new_client_id)){
                $reg->client_id = $request->data->new_client_id;
            }
            $reg->billing_date = $tmp;
            $reg->updated_at = $updated_at;  
            $reg->save(); 
            return $reg;
        }else{
            return "error";
        }
    }

    public static function delete($id){
        $reg = Billing::find($id);
        if($reg){
            $reg->delete();
            return $reg;
        }else{
            return 'error';
        }         
    }

    public static function exists($code = null){ 
        $find = false;
        if($code != null){
            $find = Billing::where('code', $code)->first();
        }
        return $find;
        
    }
}


class BillingItemController{

    const REGS_X_PAGE = 20;

    public static function &applyGlobalSearch(&$builder,$search){ 
        /*
        $fields = [
            "param1",
            "param2",
        ];
        foreach ($fields as $key => $value) {
             $builder->orWhere($value,'like',"%$search%");
        }
        */
        return $builder;
    }

    public static function &getItemsByBill($billing_id){
        $regs = BillingItem::where("billing_id",$billing_id)->get();
        $regs = ["data"=>$regs,"billing_id"=>$billing_id];
        return $regs;
    } 
    public static function &allItemsByBill(&$request){
        global $url_vehicles;
        $search = $request->data->search;

        $regs = BillingItem::where("billing_id",$request->data->billing_id)->get(); 


        $vehicles = [];

        $curl = new Curl();
        foreach ($regs as $key => &$item) {   
            $vehicle_id = $item->vehicles_id;
            if(array_key_exists($vehicle_id,$vehicles))continue; 
            $url = $url_vehicles.$vehicle_id;
            $response = $curl->get($url);   
            if(!$curl->error){
                $vehicles[$vehicle_id] = $curl->response; 
            }else{
                $vehicles[$vehicle_id] = $curl->errorCode . ': ' . $curl->errorMessage;
            }  
        }
        $vehicles_filter = [];
        foreach($vehicles AS $key => &$veh){ 
            if(
                stripos($veh->plate,$search)!==false || 
                stripos($veh->simcard,$search)!==false || 
                stripos($veh->imei,$search)!==false || 
                stripos($veh->name,$search)!==false || 
                stripos($veh->brand,$search)!==false || 
                stripos($veh->model,$search)!==false || 
                stripos($veh->phone,$search)!==false || 
                stripos($veh->protocol,$search)!==false
            ){
                //guardar el vínculo de este item con vehículo
                $vehicles_filter[$veh->id] = true;
            }
        }

        $regs = [
            "items"=>$regs,
            "vehicles"=>$vehicles,
            "vehicles_filter"=>$vehicles_filter,
            "vehicles_filter_length"=>count($vehicles_filter),
        ];


        return $regs;
    } 

    public static function getAllByBill($billing_id,$page,$request){  
        global $url_clients;
        if($page>0){
            $_method = getMethodArray();
            $global_search  = $request->data->global_search;
            $filter_field   = $request->data->filter_field;
            $filter_order   = $request->data->filter_order;//1 asc, other desc 
            $builder = Billing::offset( ( $page - 1 ) * self::REGS_X_PAGE )->limit( self::REGS_X_PAGE );
            if($global_search<>""){
                $builder = &BillingItemController::applyGlobalSearch($builder,$global_search);
            }
            if($filter_order!=0){
                $builder->orderBy($filter_field,$filter_order==1 ? "ASC":"DESC");
            }
            //return $builder->toSql(); 

            $data = $builder->get();
            $total_reg = BillingItemController::count( null , false , $global_search);
            $total_pages = floor( $total_reg / ( float ) self::REGS_X_PAGE ) + ( $total_reg % self::REGS_X_PAGE > 0  ?  1 : 0);
             

            $res = [  
                "data" => $data , 
                "details"=>[
                    "regs_x_page" => self::REGS_X_PAGE,
                    "total_pages" => $total_pages , 
                    "total_regs" => $total_reg,
                    "current_page" => $page,
                ], 
            ];
        }else{
            $res = [];
        }
        return $res;
    }
    public static function countByBill($billing_id,$return_arr = true,$global_search = ""){ 
        if(is_numeric($billing_id)){
            $builder = Billing::where( "billing_id" , $billing_id );
            if($global_search<>""){
                $builder = &BillingItemController::applyGlobalSearch($builder,$global_search);
            }
            $total = $builder->count(); 
        }else{
            $total = 0; 
        } 
        if( $return_arr ) return [ "count" => $total ]; 
        return $total;
    }
    public static function create($request){
        $contact = BillingItem::create($request->data->getData());
        return $contact;
    } 
    public static function update($request, $id){
        $_data = $request->data->getData(); 
        $updated_at = date("Y-m-d H:i:s");
        $reg = Billing::find($id);
        if($reg){
            foreach ($_data as $key => $value)$reg[$key]  = $value;
            $reg->updated_at = $updated_at;  
            $reg->save(); 
            return $reg;
        }else{
            return "error";
        }
    }
    public static function &delete($id){
        $reg = BillingItem::find($id);
        if($reg){
            $reg->delete(); 
        }else{
            $reg = 'error';
        }  
        return $reg;
    }
    public static function &add(&$request){
        if(!is_numeric($request->data->price)){
            $request->data->price = 0;
        }
        $reg = BillingItem::create([ 
            'vehicles_id' => $request->data->vehicles_id, 
            'billing_id' => $request->data->billing_id, 
            'price' => $request->data->price,  
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]); 
        return $reg;
    }
}