DROP TABLE IF EXISTS `billing`;
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL, 
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'codigo de factura de sunat como E001-4003',
  `billing_date` date, 
  `doc_type` TINYINT UNSIGNED NOT NULL DEFAULT 0, 
  `type` TINYINT UNSIGNED NOT NULL DEFAULT 1, 
  `cycle` TINYINT UNSIGNED NOT NULL DEFAULT 1, 
  `price` decimal(9,2) UNSIGNED NOT NULL DEFAULT 0, 
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `billing_items`;
CREATE TABLE IF NOT EXISTS `billing_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `billing_id` int(10) UNSIGNED NOT NULL, 
  `vehicles_id` int(10) UNSIGNED NOT NULL, 
  `price` decimal(9,2) UNSIGNED NOT NULL DEFAULT 0, 
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `billing_items_unique` (`billing_id`,`vehicles_id`),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE VIEW v_charging AS SELECT billing.id , billing.client_id , billing.billing_date, 
(CASE
    WHEN `cycle` = 20 OR `cycle` = 0 THEN SUM(billing_items.price) + billing.price
    ELSE SUM(billing_items.price)
END) AS amount
FROM `billing` INNER JOIN `billing_items`  ON `billing`.`id` = `billing_items`.`billing_id` GROUP BY billing.id;
