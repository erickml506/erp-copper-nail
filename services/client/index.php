<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}
/*
|--------------------------------------------------------------------------
| ROUTES CRUD
|--------------------------------------------------------------------------
|
| Here is where the app listen all request and make actions
| CREATE, READ, UPDATE, DELETE
| 
|--------------------------------------------------------------------------
| GET ALL CLIENTS
|--------------------------------------------------------------------------
*/

Flight::route('GET /all', function(){
    $clients =  ClientController::getAllClientss();
    Flight::json($clients, 200);
 });

Flight::route('GET|POST /findByName', function(){ 
    $request = Flight::request();
    $clients =  &ClientController::findByName($request);
    Flight::json($clients, 200);
 });

Flight::route('GET|POST /findByUsername', function(){ 
    $request = Flight::request();
    $reg =  &ClientController::findByUsername($request);
    Flight::json($reg, 200);
 });

/*
|--------------------------------------------------------------------------
| GET ONE CLIENTS
|--------------------------------------------------------------------------
*/

Flight::route('GET /@id', function($id)  {
    $client = ClientController::getClient($id);
    $client ? Flight::json($client, 200) : Flight::json(['error' => "No content"], 204);
});
/*
|--------------------------------------------------------------------------
| GET CLIENTS BY NAME
|--------------------------------------------------------------------------
*/
Flight::route('GET /getIDsWhereName/@name:[\w]{1,150}', function($name){
    $clients =  ClientController::getClientsIDsWhereName($name);
    Flight::json($clients, 200);
 });
/*
|--------------------------------------------------------------------------
| CREATE CLIENTS
|--------------------------------------------------------------------------
*/
Flight::route('POST /create', function()  {
    $request = Flight::request();
    $document = $request->data->document;
    $client = ClientController::createClient($request, $document);
    $client == 'error' ? Flight::json(['error' => "Duplicated client"], 204) : Flight::json($client, 201);
});


/*
|--------------------------------------------------------------------------
| UPDATE CLIENTS
|--------------------------------------------------------------------------
*/
Flight::route('POST /update', function() {
    $request = Flight::request();
    $client = ClientController::UpdateClient($request, $request->data->id);
    $client == 'error' ? Flight::json(['error' => "No content"], 204) : Flight::json($client, 200);
});

/*
|--------------------------------------------------------------------------
| DETELE CLIENTS
|--------------------------------------------------------------------------
*/
Flight::route('POST /delete', function()  {
    $request = Flight::request();
    $client = ClientController::deleteClient($request->data->id);
    $client == 'error' ? Flight::json(['error' => "No content"], 204) : Flight::json($client, 200);
});


/*
|--------------------------------------------------------------------------
| CRUD CONTACTS
|--------------------------------------------------------------------------

|--------------------------------------------------------------------------
| GET CONTACTS BY CLIENT
|--------------------------------------------------------------------------
*/
Flight::route('POST /getContacts', function(){
    $request = Flight::request();
    $contacts = ContactController::getContactsByClient($request->data->id_client);
    $contacts == 'error' ? Flight::json(['error' => "No content"], 204) : Flight::json($contacts, 200);
});
/*
|--------------------------------------------------------------------------
| UPDATE CLIENTS
|--------------------------------------------------------------------------
*/
Flight::route('POST /editContact', function() {
    $request = Flight::request();
    $contact = ContactController::updateContact($request, $request->data->id);
    $contact == 'error' ? Flight::json(['error' => "No content"], 204) : Flight::json($contact, 200);
});
/*
|--------------------------------------------------------------------------
| CREATE CONTACTS
|--------------------------------------------------------------------------
*/
Flight::route('POST /createContact', function()  {
    $request = Flight::request();
    $contact = ContactController::createContact($request);
    Flight::json($contact, 201);

});
/*
|--------------------------------------------------------------------------
| DELETE CONTACTS
|--------------------------------------------------------------------------
*/
Flight::route('POST /deleteContact', function()  {
    $request = Flight::request();
    $contact = ContactController::deleteContact($request->data->id);
    $contact == 'error' ? Flight::json(['error' => "No content"], 204) : Flight::json($contact, 200);
});
