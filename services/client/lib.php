<?php
use \Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Client extends Model {
    
    protected $table = 'clients';
    protected $fillable = [
        'type',
        'document', 
        'business_name', 
        'fiscal_address',
        'install_address',
        'install_hours',
        'number_vehicles', 
        'user',
        'password' ,
        'price_gps' ,
        'price_plataform',
        'is_rented',
        'rent_expiration',
        'purchase_order' ,
        'payment_method',
        'operation_number',
        'service_type',
        'status'
    ];
}
class ClientController {

    public static function &findByName(&$request){
        $name = $_GET["name"];
        if(!isset($name) || strlen($name)==0){
            $name = $request->data->name;
        }
        if(strlen($name)<3){
            $res = "";
        }else{
            $res = Client::where("business_name","like","%".$name."%")->get();
        } 
        $res = [
            "data"=>$res,
            "name"=>$name,  
        ];
        return $res;
    }
    public static function &findByUsername(&$request){
        $username = @$_GET["username"];
        if(!isset($username) || strlen($username)==0){
            $username = $request->data->username;
        }
        if(strlen($username)<3){
            $res = "";
        }else{
            $res = Client::where("user","=",$username)->first();
        } 
        $res = [
            "data"=>$res,
            "name"=>$username,  
        ];
        return $res;
    }

    public static function getAllClientss(){
        $clients = Conexion::getData('clients','id');
        return $clients;
    }
    
    public static function getClient($id){
        $client = Client::find($id);
        return $client;
    }
    public static function getClientsIDsWhereName($str){
        $client = Client::where("business_name","like","%$str%")->get();
        return $client;
    }

    public static function requestData($request, $action){
        $data = [
            'type'=> $request->data->type,
            'document'=> $request->data->document,
            'business_name'=> $request->data->business_name,
            'fiscal_address'=> $request->data->fiscal_address,
            'install_address' => $request->data->install_address,
            'install_hours' => $request->data->install_hours,
            'number_vehicles' => $request->data->number_vehicles,
            'user' => $request->data->user,
            'price_gps' => $request->data->price_gps,
            'price_plataform' => $request->data->price_plataform,
            'is_rented' => $request->data->is_rented,
            'rent_expiration' => $request->data->rent_expiration,
            'purchase_order' => $request->data->purchase_order,
            'payment_method' => $request->data->payment_method,
            'operation_number' => $request->data->operation_number,
            'service_type' => $request->data->service_type
        ];
        if($action == 'create'){
            $password = $request->data->password;
            $data['password'] = $password;
        }else if ($action == 'edit'){
            if($request->data->password !== ""){
                $password = $request->data->password;
                $data['password'] = $password;
            }
        }
        return $data;
    }

    public static function createClient($request, $document){
        $findClient = Client::where('document', $document)->first();
        if($findClient){
            return 'error';
        }else{
            $client = Client::create(self::requestData($request, 'create'));
            return $client;
        }
    }

    public static function UpdateClient($request, $id){
        $client = Client::find($id);
        if($client){
            $data = self::requestData($request, 'edit');
            Client::where('id', $id)->update($data);
            $client_updated = Client::find($id); 
            return $client_updated;
        }else{
            return 'error';
        }
    }

    public static function deleteClient($id){
        $client = Client::find($id);
        if($client){
            $client->delete();
            return $client;
        }else{
            return 'error';
        }         
    }

}

class Contact extends Model {
    
    protected $table = 'contacts';
    protected $fillable = [
        'id_client', 'name', 'phone', 'relationship', 'email', 'type'
    ];
}

class ContactController{

    public static function getContactsByClient($id_client){
        //require 'data.php';
        $contacts = Conexion::getData('contacts','id', $id_client);
        if(count($contacts["data"]) > 0){
            return  $contacts;
        }else{
            return 'error';
        }
    }
    public static function createContact($request){
        $contact = Contact::create(self::requestData($request));
        return $contact;
    }
    public static function requestData($request){
        return [
            'name' => $request->data->name,
            'id_client' => $request->data->id_client,
            'phone'=> $request->data->phone,
            'email' => $request->data->email,
            'relationship' => $request->data->relationship,
            'type' => $request->data->type
        ];
    }
    public static function updateContact($request, $id){
        $contact = Client::find($id);
        if($contact){
            $data = self::requestData($request);
            Contact::where('id', $id)->update($data);
            $contact_updated = Contact::find($id); 
            return $contact_updated;
        }else{
            return 'error';
        }
    }
    public static function deleteContact($id){
        $contact = Contact::find($id);
        if($contact){
            $contact->delete();
            return $contact;
        }else{
            return 'error';
        }  
    }
}

?>