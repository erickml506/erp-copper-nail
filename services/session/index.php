<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}


/*
|--------------------------------------------------------------------------
| GET ALL SESSIONS
|--------------------------------------------------------------------------
*/
Flight::route('GET /all', function()  {
    $sessions = SessionController::getAllSessions();
    Flight::json($sessions, 200);
});
/*
|--------------------------------------------------------------------------
| CREATE SESSIONS
|--------------------------------------------------------------------------
*/
Flight::route('POST /', function(){
    $request = Flight::request();
    $session = SessionController::create($request);
    if($session == 'error'){
        Flight::json(['error' => "Duplicated session"], 409);
    }else{
        Flight::json($session, 201);
    }
});
/*
|--------------------------------------------------------------------------
| CHECK SESSIONS:
|--------------------------------------------------------------------------
*/
Flight::route('POST /check', function(){
    $request = Flight::request();
    $session = SessionController::checkSession($request);
    if($session == 'error'){
        Flight::json(['error' => "No content"], 403);
    }else if($session == 'expired'){
        Flight::json(['valid' => "false"], 401);
    }else if($session == 'valid'){
        Flight::json(['valid' => "true"], 200);
    }
});
/*
|--------------------------------------------------------------------------
| DESTROY SESSIONS
|--------------------------------------------------------------------------
*/
Flight::route('POST /destroy', function(){
    $request = Flight::request();
    $session = SessionController::destroy($request);
    if($session == 'error'){
        Flight::json(['error' => "No content"], 204);
    }else{
        Flight::json($session, 200);
    }
});
/*
|--------------------------------------------------------------------------
| DESTROY ALL SESSIONS (HELPER FOR TESTS)
|--------------------------------------------------------------------------
*/
Flight::route('DELETE /deleteAll', function(){
    SessionController::deleteAll();
});
