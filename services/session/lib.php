<?php
date_default_timezone_set('America/Lima');
require 'GibberishAES.php';
use \Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
class Session extends Model {
    
    protected $table = 'session';
    protected $fillable = [
        'id_user', 'api_token', 'time_expiration'
    ];
}

class SessionController {

    public static $key = "kyO7v2mihIl18Q8RA6hsj3kf3arPPQgKhvYV2jy9hVKrHzyxGOeg8Tjq7eh1";

    public static function getToken($length = 60){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public static function checkSession($request){
        $encrypted_session = $request->data->session;
        $decrypted_session = GibberishAES::dec($encrypted_session, Self::$key);
        $session = json_decode($decrypted_session);
        $id_user = $session->id_user;
        $api_token = $session->api_token;
        $time_expiration = $session->time_expiration;
        $create_at = $session->created_at;
        $session = Session::where('id_user', $id_user)->where('api_token', $api_token)->first();
        $time = date("Y-m-d H:i:s");
        if($session){
            if($time_expiration > $time) {
                return 'valid';
            }else{
                return 'expired';
            }
        }else{
            return 'error';
        }
       
    }
    public static function getAllSessions(){
        $sessions = Session::all();
        return $sessions;
    }
    public static function getTimeExpiration($time){
        $timeplus = strtotime ('+3 hours' , strtotime ($time)) ;
        $time_expiration = date ("Y-m-d H:i:s" , $timeplus );
        return $time_expiration;
    }
    //no se usara por ahora
    public static function UpdateTimeExpiration($id){
        $session = Session::find($id);
        if($session){
            $time = date("H:i:s");
            $session->time_expiration = Self::getTimeExpiration($time);
            $session->save();
            return $session;
        }else{
            return 'error';
        }
    }

    public static function create($request){
        $id_user = $request->data->id;
        $api_token = $request->data->api_token;
        $session = Session::where('id_user', $id_user)->where('api_token', $api_token)->first();
        if($session){
            return 'error';
        }else{
            $time = date("Y-m-d H:i:s");
            $time_expiration = Self::getTimeExpiration($time);
            $session = Session::create([
                'id_user' => $id_user,
                'api_token' => $api_token,
                'time_expiration' => $time_expiration
            ]);
            $session = GibberishAES::enc(json_encode($session), Self::$key); 
            return $session;
        }
    }


    public static function destroy($request){
        $encrypted_session = $request->data->session;
        $decrypted_session = GibberishAES::dec($encrypted_session, Self::$key);
        $session = json_decode($decrypted_session);
        $id = $session->id;
        $id_user = $session->id_user;
        $session = Session::find($id);
        if($session){
            Session::where('id_user', $id_user)->delete();
            $session->delete();
            return $session;
        }else{
            return 'error';
        }
    }

    public static function deleteAll(){
        $sessions = Session::whereNotNull('id')->delete();
    }
}


