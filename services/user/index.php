<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}

/*
|--------------------------------------------------------------------------
| ROUTES CRUD
|--------------------------------------------------------------------------
|
| Here is where the app listen all request and make actions
| CREATE, READ, UPDATE, DELETE
| 
|--------------------------------------------------------------------------
| GET ALL USERS
|--------------------------------------------------------------------------
*/
Flight::route('GET /all', function(){
    $users = UserController::getAllUsers();
    Flight::json($users, 200);
 });

/*
|--------------------------------------------------------------------------
| GET ONE USER
|--------------------------------------------------------------------------
*/

Flight::route('GET /@id', function($id)  {
    $user = UserController::getUser($id);
    if($user){
        Flight::json($user, 200);
    }else{
        Flight::json(['error' => "No content"], 204);
    }
});

/*
|--------------------------------------------------------------------------
| CREATE USER
|--------------------------------------------------------------------------
*/
Flight::route('POST /create', function()  {
    $request = Flight::request();
    $user = UserController::createUser($request, $request->data->email);
    $user == 'error' ? Flight::json(['error' => "Duplicated user"], 204) : Flight::json($user, 201);
});


/*
|--------------------------------------------------------------------------
| UPDATE USER
|--------------------------------------------------------------------------
*/
Flight::route('POST /update', function() {
    $request = Flight::request();
    $user = UserController::UpdateUser($request, $request->data->id);
    if($user == 'error'){
        Flight::json(['error' => "No content"], 204);
    }else{
        Flight::json($user, 200);
    }
});

/*
|--------------------------------------------------------------------------
| DETELE USER
|--------------------------------------------------------------------------
*/
Flight::route('POST /delete', function()  {
    $request = Flight::request();
    $user = UserController::deleteUser($request->data->id);
    if($user == 'error'){
        Flight::json(['error' => "No content"], 204);
    }else{
        Flight::json($user, 200);
    }
});

/*
|--------------------------------------------------------------------------
| VALIDATE USER
|--------------------------------------------------------------------------
*/
Flight::route('POST /validate_user', function()  {
    $request = Flight::request();
    $email = $request->data->email;
    $password = $request->data->password;
    $user = UserController::validation_user($email, $password);
    if($user === 'error'){
        Flight::json(['error' => 'Unauthorize'], 403);
    }else{
        Flight::json($user, 200);
    }
});

/*
|--------------------------------------------------------------------------
| RUN APPLICATION
|--------------------------------------------------------------------------
*/
