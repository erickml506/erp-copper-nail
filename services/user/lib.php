<?php
use \Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    protected $table = 'users';
    protected $fillable = [
        'name', 'apellidos', 'email', 'type', 'api_token', 'password'
    ];
    /* comment for test
    protected $hidden = [
        'password',
    ];*/

}

class UserController {

    protected $key = "0YOjHt3Gp19oHVXi12WSVr2";

    public static function getToken($length = 60){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public static function getAllUsers(){
        //require 'data.php';
        $users = Conexion::getData('users','id');
        return $users;
    }
    
    public static function getUser($id){
        $user = User::find($id);
        return $user;
    }
    //no tested yet
    public static function validation_user($email, $password){
        $user = User::where('email', $email)->first();
        if(password_verify($password, $user->password)){
            $user->api_token = Self::getToken();
            $user->save();
            return $user;
        }else{
            return 'error';
        }
    }

    public static function createUser($request, $email){
        $findUser = User::where('email', $email)->first();
        if($findUser){
            return 'error';
        }else{
            $user = User::create(Self::requestData($request, 'create'));
            return $user;
        }
       
    }
    public static function requestData($request, $action){
        $data = [
            'name'=> $request->data->name,
            'apellidos'=> $request->data->apellidos,
            'email'=> $request->data->email,
            'type' => $request->data->type,
        ];
        if($action == 'create'){
            $password = password_hash($request->data->password, PASSWORD_DEFAULT);
            $api_token = self::getToken();
            $data['api_token'] = $api_token;
            $data['password'] = $password;
        }else if ($action == 'edit'){
            $api_token = self::getToken();
            $data['api_token'] = $api_token;
            if($request->data->repassword !== ""){
                $password = password_hash($request->data->password, PASSWORD_DEFAULT);
                $data['password'] = $password;
            }
        }
        return $data;
    }
    public static function UpdateUser($request, $id){
        $user = User::find($id);
        if($user){
            $data = self::requestData($request, 'edit');
            User::where('id', $id)->update($data);
            $user_updated = User::find($id); 
            return $user_updated;
        }else{
            return 'error';
        }
    }

    public static function deleteUser($id){
        $user = User::find($id);
        if($user){
            $user->delete();
            return $user;
        }else{
            return 'error';
        }         
    }

}


