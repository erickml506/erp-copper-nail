<?php  
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}

Flight::route('GET /hello', function(){
    Flight::json(["hello",'world'], 200);
});

Flight::route('PUT|POST /push', function(){
    $_method = getMethodArray();
    $request = Flight::request();
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    } 
    $exec = VehiclesSyncController::parse($request);
    Flight::json(["res"=>$exec], 200);
});

//http PUT api.geointranet/v1/vehicles-sync/push username=eeeee@aaaab.com imei=333333 name=22222222 brand=gggg model=ttttttt plate=gggg phone=444444 simcard=33333 protocol=ddddd

//http PUT gateway.geointranet/syncVehicle username=eeeee@aaaab.com imei=333333 name=22222222 brand=gggg model=ttttttt plate=gggg phone=444444 simcard=33333 protocol=ddddd

