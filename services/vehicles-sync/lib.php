<?php
# here write your classes
require "php-curl-class-master/src/Curl/ArrayUtil.php";
require "php-curl-class-master/src/Curl/CaseInsensitiveArray.php";
require "php-curl-class-master/src/Curl/Curl.php";
require "php-curl-class-master/src/Curl/Decoder.php";
require "php-curl-class-master/src/Curl/MultiCurl.php";
require "php-curl-class-master/src/Curl/StrUtil.php";
require "php-curl-class-master/src/Curl/Url.php";
 
use \Curl\Curl;

use \Illuminate\Database\Eloquent\Model; 

class VehiclesSync extends Model { 
    
    protected $table = 'vehicles';
    protected $fillable = [
        '_id',
        'username', 
        'client_id', 
        'imei', 
        'name', 
        'brand', 
        'model', 
        'plate', 
        'phone', 
        'simcard',
        'protocol',
    ];
    protected $hidden = [  
    ];

}

class VehiclesSyncController {
	public static function parse($request){  
        global $url_clients;

        $curl = new Curl();
     	$url = $url_clients."findByUsername";
        $response = $curl->post($url,array(
        	"username"=>$request->data->username
        ));  

        $client = null;

		try{
	        if(!$curl->error){
	            $client = $curl->response;
	            $data = @$client->data;
	            $client_id = $data->id;
	        }else{
	        	$client_id = 0;
	    	}
    	}catch(\Exception $e){
			return $e->getMessage();
    	}


		$last = 
		VehiclesSync::whereRaw("username='{$request->data->username}'")
		->whereRaw("plate='{$request->data->plate}'")
		->first();

		//$last = VehiclesSync::where("username",$request->data->username)->first();
 		if($last){ 
				$last->client_id = $client_id;
				$last->imei = $request->data->imei;
				$last->name = $request->data->name;
				$last->brand = $request->data->brand;
				$last->model = $request->data->model;
				$last->plate = $request->data->plate;
				$last->phone = $request->data->phone;
				$last->simcard = $request->data->simcard;
				$last->protocol = $request->data->protocol;
			try{
				$last->save();
				return [
					"condition"	=> 'update',
					"username"	=> $request->data->username,
					"client_id" => $client_id,
					"vehicle_id" => $last->id,
					"client_info" => $client->data,
				];
			}catch(\Exception $e){
				return $e->getMessage();
			}
		}else{
			try{
					$vehicle = VehiclesSync::create([
			            'client_id' => $client_id,
			            'username' => $request->data->username,
			            'imei' => $request->data->imei,
			            'name' => $request->data->name,
			            'brand' => $request->data->brand,
			            'model' => $request->data->model,
			            'plate' => $request->data->plate,
			            'phone' => $request->data->phone,
			            'simcard' => $request->data->simcard, 
			            'protocol' => $request->data->protocol, 
			            'created_at' => date("Y-m-d H:i:s"),
			            'updated_at' => date("Y-m-d H:i:s"), 
			        ]);
					return [
						"condition"	=> 'insert',
						"username"	=> $request->data->username,
						"client_id" => $client_id,
						"vehicle_id" => $last->id,
						"client_info" => $client->data,
					];
			}catch(\Exception $e){
				return $e->getMessage();
			}
		}

	}
}
