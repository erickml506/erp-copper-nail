<?php  
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}
/*
|--------------------------------------------------------------------------
| ROUTES CRUD
|--------------------------------------------------------------------------
|
| Here is where the app listen all request and make actions
| CREATE, READ, UPDATE, DELETE
| 
|--------------------------------------------------------------------------
| GET ALL VEHICLES
|--------------------------------------------------------------------------
*/ 

Flight::route('GET /all(/@page:[0-9]{1,9}(/@client_id:[0-9]{1,9}))', function($page=1,$client_id = null){ 
    if(!is_numeric($page) || $page<1){ 
        $_method = getMethodArray(); 
        $page = getValueFromArray("page",$_method);
        if(!is_numeric($page) || $page<1)$page=1;
    }else{
        $page=1;
    } 
    if(is_numeric($client_id)){
        $reg = VehicleController::getAllByClient($client_id,$page);
    }else{
        $reg = VehicleController::getAll((int)$page);
    }
    
    Flight::json($reg, 200);
 });

Flight::route('POST /getVehiclesByClient', function(){
    $request = Flight::request();
    $vehicles = VehicleController::getVehiclesByClient($request->data->id);
    Flight::json($vehicles, 200);
 });


Flight::route('GET /count(/@client_id:[0-9]{1,9})', function($client_id){ 
    $_method = getMethodArray(); 
    if(!isset($client_id)){ 
        $client_id = getValueFromArray("client_id",$_method);
    }  
    Flight::json(VehicleController::count($client_id), 200);
});

/*
|--------------------------------------------------------------------------
| GET ONE VEHICLE
|--------------------------------------------------------------------------
*/

Flight::route('GET /(@id)', function($id)  { 
    $_method = getMethodArray(); 
    if(!is_numeric($id)){
        $id = getValueFromArray("id",$_method);
    }  
    $vehicle = VehicleController::get($id);
    if($vehicle){
        Flight::json($vehicle, 200);
    }else{
        Flight::json(['error' => "No content"], 204);
    }
    
});

/*
|--------------------------------------------------------------------------
| CREATE VEHICLE
|--------------------------------------------------------------------------
*/
Flight::route('POST /create', function()  {
    $_method = getMethodArray();
    $request = Flight::request();
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    }
    $sim_card = $request->data->sim_card;
    $if_exists = VehicleController::exists($sim_card);
    if($if_exists){
        Flight::json(['error' => "Duplicated simcard vehicle. Please change data"], 204);
    }else{ 
        $reg = VehicleController::create($request);
        Flight::json($reg, 201);
    }
});


/*
|--------------------------------------------------------------------------
| UPDATE VEHICLE
|--------------------------------------------------------------------------
*/
Flight::route('PUT /update(/@id)', function($id) {  
    $_method = getMethodArray();
    if(!is_numeric($id)){
        $id = getValueFromArray("id",$_method);
        if(array_key_exists("session",$_method))unset($_method["session"]);  
        if(array_key_exists("password",$_method))unset($_method["password"]);   
        if(array_key_exists("id",$_method))unset($_method["id"]);   
    }  
    
    $request = Flight::request();
    if(is_array($_method) && count($_method)>0){  
        $request->data->setData($_method);
    }

    /*Flight::json([
        'error' => "No content",
        "file_contents"=>$file_contents,
        "request->data"=>$request->data->getData(),
        "imei"=>$request->data->imei,
        "id"=>$id
    ], 204); 
    return;*/
    $reg = VehicleController::update($request, $id);
    if($reg == 'error'){
        Flight::json([
            'error' => "No content",
            "_method"=>$_method
        ], 204);
    }else{
        Flight::json($reg, 200);
    }
});

/*
|--------------------------------------------------------------------------
| DETELE VEHICLE
|--------------------------------------------------------------------------
*/
Flight::route('DELETE /delete(/@id)', function($id=null)  {  
    $_method = getMethodArray();
    if(!is_numeric($id)){
        $id = getValueFromArray("id",$_method);
    }   
    $reg = VehicleController::delete($id);
    if($reg == 'error'){
        Flight::json([
            'error' => "Not found",
            "_method"=>$_method,
            "id"=>$id, 
        ], 204);
    }else{
        Flight::json($reg, 200);
    }
});  

/*
|--------------------------------------------------------------------------
| BUSCAR VEHÍCULOS EN BASE UN PATRÓN PARA SER UTILIZADO EN AGREGAR ITEM EN BILLING
|--------------------------------------------------------------------------
*/
Flight::route('POST /findBy', function()  {  
    $request = Flight::request();
    $reg = &VehicleController::findBy($request);
    if($reg == 'empty'){
        Flight::json([
            'error' => true,
            'message' => "Not found",
            "request_params"=>$request->data, 
        ], 204);
    }else{
        Flight::json(["data"=>$reg], 200);
    }
});  