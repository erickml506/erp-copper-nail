<?php
require "php-curl-class-master/src/Curl/ArrayUtil.php";
require "php-curl-class-master/src/Curl/CaseInsensitiveArray.php";
require "php-curl-class-master/src/Curl/Curl.php";
require "php-curl-class-master/src/Curl/Decoder.php";
require "php-curl-class-master/src/Curl/MultiCurl.php";
require "php-curl-class-master/src/Curl/StrUtil.php";
require "php-curl-class-master/src/Curl/Url.php";

use \Illuminate\Database\Eloquent\Model;
use \Curl\Curl;

class Vehicle extends Model {
    
    protected $table = 'vehicles';
    protected $fillable = [
        'client_id', 
        'imei', 
        'name', 
        'brand', 
        'model', 
        'plate', 
        'phone', 
        'simcard',
        'protocol',
    ];
    protected $hidden = [ 
        '_id',//core 
    ];

}

class VehicleHistory extends Model {
    
    protected $table = 'vehicles_history';
    protected $fillable = [
        '_id', 
        'vehicles_id', 
        'client_id', 
        'imei', 
        'name', 
        'brand', 
        'model', 
        'plate', 
        'phone', 
        'simcard',
        'protocol',
    ];
    protected $hidden = [ 
        '_id',//core 
    ];

}

class VehicleController {

    const REGS_X_PAGE = 20;

    public static function applyGlobalSearch($builder,$search){
        $fields = ["imei","name","brand","model","plate","phone","simcard","protocol"];
        foreach ($fields as $key => $value) {
             $builder->orWhere($value,'like',"%$search%");
        }
        return $builder;
    }

    public static function getAll($page){  
        global $url_clients;
        if($page>0){
            $_method = getMethodArray();
            $global_search  = getValueFromArray("global_search",$_method);
            $filter_field   = getValueFromArray("filter_field",$_method);
            $filter_order   = getValueFromArray("filter_order",$_method);//1 asc, other desc
            $builder = Vehicle::offset( ( $page - 1 ) * self::REGS_X_PAGE )->limit( self::REGS_X_PAGE );
            if($global_search<>""){
                $builder = VehicleController::applyGlobalSearch($builder,$global_search);
            }
            if($filter_order!=0){
                $builder->orderBy($filter_field,$filter_order==1 ? "ASC":"DESC");
            }
            //return $builder->toSql(); 

            $data = $builder->get();
            $total_reg = VehicleController::count( null , false , $global_search);
            $total_pages = floor( $total_reg / ( float ) self::REGS_X_PAGE ) + ( $total_reg % self::REGS_X_PAGE > 0  ?  1 : 0);
            
            $clients = [];
            
            $curl = new Curl();
            foreach ($data as $key => &$value) {
                $client_id = $value["client_id"];
                if(array_key_exists($client_id,$clients))continue;
                $url = $url_clients.$client_id;
                $response = $curl->get($url); 
                /*$value["url"] = $url_clients;
                $value["error"] = $curl->error;
                $value["errorCode"] = $curl->errorCode;
                $value["errorMessage"] = $curl->errorMessage; */
                if(!$curl->error){
                    $clients[$client_id] = $curl->response;
                }
            }
            foreach ($clients as $key => &$value) {
                $tmp = (array)$value;
                foreach ($tmp as $key1 => &$value1) {
                    if($key1=="business_name")continue;
                    unset($tmp[$key1]);
                }
                $clients[$key] = $tmp;
                
            }

            $res = [ 
                "clients" => $clients , 
                "data" => $data , 
                "details"=>[
                    "regs_x_page" => self::REGS_X_PAGE,
                    "total_pages" => $total_pages , 
                    "total_regs" => $total_reg,
                    "current_page" => $page,
                ],
                "get"=>$_GET,
                "post"=>$_POST,
                "phpinput"=>getPhpInputData(),
            ];
        }else{
            $res = [];
        }
        return $res;
    }
    public static function getAllByClient($client_id,$page){
        if($client_id>0){
            $global_search = getValueFromArray("global_search",$_GET);
            $filter_field   = getValueFromArray("filter_field",$_GET);
            $filter_order   = getValueFromArray("filter_order",$_GET);//1 asc, other desc
            if($page>0){
                $builder = Vehicle::where( "client_id", $client_id )->offset( ( $page - 1 ) * self::REGS_X_PAGE )->limit( self::REGS_X_PAGE );
            }else{
                $builder = Vehicle::where( "client_id", $client_id );
            }
            if($global_search<>""){
                $builder = VehicleController::applyGlobalSearch($builder,$global_search);
            }
            if($filter_order!=0){
                $builder->orderBy($filter_field,$filter_order==1 ? "ASC":"DESC");
            }
            $data = $builder->get();
            if($page>0){ 
                $total_reg = VehicleController::count( $client_id , false , $global_search );
                $total_pages = floor( $total_reg / ( float ) self::REGS_X_PAGE ) + ( $total_reg % self::REGS_X_PAGE > 0  ?  1 : 0);
                $res = [ 
                    "data" => $data , 
                    "details"=>[
                        "regs_x_page" => self::REGS_X_PAGE,
                        "total_pages" => $total_pages , 
                        "total_regs" => $total_reg,
                        "current_page" => $page,
                    ],
                    "extra"=>$_GET,
                ];
            }else{
                $res = [ 
                    "data" => $data , 
                    "details"=>[ 
                    ],
                    "get"=>$_GET,
                    "post"=>$_POST,
                    "phpinput"=>getPhpInputData(),
                ];
            }
        }else{
            $res = [];
        }
        return $res;
    }
    public static function getVehiclesByClient($client_id){
        $vehicles = Conexion::getData('vehicles','id', $client_id);
        return  $vehicles;

    }
    public static function count($client_id,$return_arr = true,$global_search = ""){
        global $capsule;
        if($client_id){
            $builder = Vehicle::where( "client_id" , $client_id );
            if($global_search<>""){
                $builder = VehicleController::applyGlobalSearch($builder,$global_search);
            }
            $total = $builder->count(); 
        }else{
            $builder = Vehicle::query();
            if($global_search<>""){
                $builder = VehicleController::applyGlobalSearch($builder,$global_search);
            }
            $total = $builder->count(); 
        } 
        if( $return_arr ) return [ "count" => $total ]; 
        return $total;
    }
    
    public static function get($id){
        $vehicle = Vehicle::find($id);
        return $vehicle;
    }  
    public static function &findBy(&$request){
        $_search = $request->data->search;
        $regs = Vehicle::where("plate","LIKE","%$_search%")->get();
        if(!$regs)$regs="empty";
        return $regs;
    }  
    public static function create($request){
        $vehicle = Vehicle::create([
            'imei' => $request->data->imei,
            'name' => $request->data->name,
            'brand' => $request->data->brand,
            'model' => $request->data->model,
            'plate' => $request->data->plate,
            'phone' => $request->data->phone,
            'simcard' => $request->data->simcard, 
            'protocol' => $request->data->protocol,
            'client_id' => $request->data->client_id, 
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        return $vehicle;
    }

    public static function update($request, $id){  
        /*$body = explode("&",$request->getBody());
        $_data = [];
        if(is_array($body)){ 
            foreach ($body as $key => $value) {
                $value = explode("=",$value);
                $_data[$value[0]] = urldecode($value[1]);
            }
            //$request->data->setData($data);
        }*/
        $_data = $request->data->getData();
        //return [['body' => $request->getBody(),'data1'=>$request->data,'data2'=>$_data], 200];
        $updated_at = date("Y-m-d H:i:s");
        $vehicle = Vehicle::find($id);
        if($vehicle){

            if(
                $vehicle->imei!=$request->data->imei || 
                $vehicle->simcard!=$request->data->simcard || 
                $vehicle->plate!=$request->data->plate 
            ){ 

                $arr_history = [
                    'vehicles_id' => $vehicle->id,
                    '_id' => $vehicle->_id,
                    'imei' => $vehicle->imei,
                    'name' => $vehicle->name,
                    'brand' => $vehicle->brand,
                    'model' => $vehicle->model,
                    'plate' => $vehicle->plate,
                    'phone' => $vehicle->phone,
                    'simcard' => $vehicle->simcard, 
                    'protocol' => $vehicle->protocol,
                    'client_id' => $vehicle->client_id, 
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"), 
                ];
                VehicleHistory::create($arr_history);
            }

            foreach ($_data as $key => $value) {
                $vehicle[$key]  = $value;
            } 
            $vehicle->updated_at    = $updated_at;

            /*$vehicle->imei          = $request->data->imei;
            $vehicle->name          = $request->data->name;
            $vehicle->brand         = $request->data->brand;
            $vehicle->model         = $request->data->model;
            $vehicle->plate         = $request->data->plate;
            $vehicle->phone         = $request->data->phone;
            $vehicle->simcard       = $request->data->simcard;
            $vehicle->protocol      = $request->data->protocol;
            $vehicle->updated_at    = $updated_at;*/

            $vehicle->save(); 
            return $vehicle;
        }else{
            return "error";
        }
    }

    public static function delete($id){
        $vehicle = Vehicle::find($id);
        if($vehicle){
            $vehicle->delete();
            return $vehicle;
        }else{
            return 'error';
        }         
    }

    public static function exists($simcard = null){
        
        $find = false;
        if($simcard != null){
            $find = Vehicle::where('simcard', $simcard)->first();
        }
        return $find;
        
    }

}