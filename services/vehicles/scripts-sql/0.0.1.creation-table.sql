DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `imei` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `plate` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group` varchar(50) COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `simcard` varchar(24) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `protocol` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicles_simcard_unique` (`simcard`),
  UNIQUE KEY `vehicles_imei_unique` (`imei`),
  UNIQUE KEY `vehicles_plate_unique` (`plate`) , 
  INDEX `vehicles_username_idx` (`username`) 
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `vehicles_history`;
CREATE TABLE IF NOT EXISTS `vehicles_history` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vehicles_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `imei` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `plate` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `simcard` varchar(24) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `protocol` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `vehicles_history_vehicles_id_idx` (`vehicles_id`),
  INDEX `vehicles_history_vehicles_simcard_idx` (`simcard`),
  INDEX `vehicles_history_vehicles_imei_idx` (`imei`),
  INDEX `vehicles_history_vehicles_plate_idx` (`plate`), 
  INDEX `vehicles_history_vehicles_username_idx` (`username`) 
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;