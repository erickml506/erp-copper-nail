docker-compose -f proxy/docker-compose.yml down
docker-compose -f services/session/docker-compose.yml down
docker-compose -f services/user/docker-compose.yml down
docker-compose -f services/client/docker-compose.yml down
docker-compose -f services/billing/docker-compose.yml down
docker-compose -f gateway/docker-compose.yml down 
docker network rm localnet
