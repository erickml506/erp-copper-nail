
var HOST_NAME = HOST_NAME ? HOST_NAME : 'http://localhost';

//temporal
var _location = window.location.pathname;
var _origin = window.location.origin;


var _session =  sessionStorage.getItem("session");
var logout = $("#logout");
/*
|--------------------------------------------------------------------------
| VALIDATION IF THE SESSION EXISTS AND IF ITS CORRECT
|--------------------------------------------------------------------------
*/
if(_session == null && _location != '/'){
    window.location.replace(origin);
    sessionStorage.setItem("expired", true);
}

/*
|--------------------------------------------------------------------------
| LOGOUT
|--------------------------------------------------------------------------
*/
logout.on("click", function(){
    let sess = sessionStorage.getItem("session")
    let data_ = {'session': sess};
    sessionStorage.removeItem("session");
    $.ajax({
        url: HOST_NAME+'/logout',
        type: 'POST',
        data: data_,
        crossDomain: true,
        success: function(res, status ,xhr){
            console.log(xhr);
        },
        error: function (responseData, textStatus, errorThrown) {
            console.log(responseData, errorThrown);
        },
        complete: function(){
            window.location.replace(origin);
        }
    })
});

function updateSession(){
    const form_login = ` 
        <form accept-charset="UTF-8" class="form_login">                          
            <div class="form-group">
                <label>Correo</label>
                <input type="email" id="email" class="form-control" placeholder="Email">
                <span id="emailError" style="font-size: 12px; color: rgb(212, 7, 43); font-style: italic; float: right; padding-top: 3px; visibility: hidden;">Email address no valid</span>
            </div>
            <div class="form-group">
                <label>Contraseña</label>
                <input type="password" id="password" class="form-control" placeholder="Password">
                <span id="passemptylError" style="font-size: 12px; color: rgb(212, 7, 43); font-style: italic; float: right; padding-top: 3px; display: none;">Password no valid</span>
            </div>
            <button id="login_submit" type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Renovar sesión</button>
        </form>`;
        setTimeout(function(){
            modalBody.append(form_login)
            modal.modal('show');
        },800);

}

//bloque temporal

const changeSessionDatatables = function(dataSettings){
    let session = sessionStorage.getItem('session');
    dataSettings.map(setting =>{
        setting.ajax.data.session = session;
    });
}
const validate_email = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
const validateFormLogin = function(){
    var email = $("#email").val(),
    pass  = $("#password").val(),
    hasError = 0;
    if(validate_email(email)){$("#emailError").css('visibility', 'hidden');}else{$("#emailError").css('visibility', 'visible'); hasError++;}
    if(pass){$("#passemptylError").css("display", "none");}else{$("#passemptylError").css("display", "block");hasError++;}
    return {hasError : hasError, email: email, pass: pass};
}
$("#modalDashboard .modal-body").on("click", "#login_submit", function(e){
    e.preventDefault();
    const validate = validateFormLogin();
    if(validate.hasError == 0){
        var data = {'email': validate.email, 'password': validate.pass};
        $.ajax({
            url: HOST_NAME+'/login',
            type: 'POST',
            crossDomain: true,
            data: data,
            success: function(res, status ,xhr){
                if(xhr.status === 201){
                    sessionStorage.removeItem("session");
                    sessionStorage.setItem("session", JSON.parse(xhr.responseText));
                    toastr_success_top_right("Sesion Actualizada", 'Notificación');
                    modal.modal('hide');
                    $.fn.dataTable ? changeSessionDatatables($.fn.dataTable.settings) : false;
                    if(sessionStorage.getItem("refreshPage")){
                        sessionStorage.removeItem("refreshPage");
                        setTimeout(function(){location.reload();},900);   
                    }
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                alert("Credenciales incorrectos");
            }
        });
    }
});