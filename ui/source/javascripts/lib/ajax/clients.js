/*
|--------------------------------------------------------------------------
| GLOBAL
|--------------------------------------------------------------------------
*/
var modal =  $('#modalDashboard'),
    modalDialog =  $('#modalDashboard .modal-dialog'),
    modalBody  = $('#modalDashboard .modal-body'),
    loading = '<i class="fa fa-spinner fa-pulse fa-fw"></i>',
    key = 'kyO7v2mihIl18Q8RA6hsj3kf3arPPQgKhvYV2jy9hVKrHzyxGOeg8Tjq7eh1';

const getFormData = function(form){
    var unindexed_array = form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function(n, i){
       indexed_array[n['name']] = n['value'];
    });
    $('input[name=password]').map(function(){
        indexed_array[this.name] = GibberishAES.enc(this.value, key);
    });
    $('input:checkbox').map(function() {
        indexed_array[this.name] = this.checked ? true : false;
    });
    return indexed_array;
};

/*
|--------------------------------------------------------------------------
| HELPERS CLIENT
|--------------------------------------------------------------------------
*/
const validateFormClient = function(data, action = 'create'){
    let nameError = $("#nameError"), documentError = $("#documentError"), 
        userError = $("#userError"), passError = $("#passError"),
        fiscalAddressError = $("#fiscalAddressError"), installAddressError = $("#installAddressError"),
        installHoursError = $("#installHoursError"), numbervError = $("#numbervError"),
        referenceError = $("#referenceError"), priceGpsError = $("#priceGpsError"),
        pricePlatError = $("#pricePlatError"), rentExpiPlatError = $("#rentExpiPlatError"),
        purchaseOrderError = $("#purchaseOrderError"), _hasError = 0;
        
        if(data.business_name == ""){nameError.css("display", "block");_hasError++;}else{nameError.css("display", "none");}
        if(data.document == ""){documentError.css("display", "block");_hasError++;}else{documentError.css("display", "none");}
        if(data.user == ""){userError.css("display", "block");_hasError++;}else{userError.css("display", "none");}
        if(data.password == ""){passError.css("display", "block");_hasError++;}else{passError.css("display", "none");}
        if(data.fiscal_address == ""){fiscalAddressError.css("display", "block");_hasError++;}else{fiscalAddressError.css("display", "none");}
        if(data.install_address == ""){installAddressError.css("display", "block");_hasError++;}else{installAddressError.css("display", "none");}
        if(data.install_hours == ""){installHoursError.css("display", "block");_hasError++;}else{installHoursError.css("display", "none");}
        if(data.number_vehicles < 0 || data.number_vehicles == ""){numbervError.css("display", "block");_hasError++;}else{numbervError.css("display", "none");}
        if(data.reference == ""){referenceError.css("display", "block");_hasError++;}else{referenceError.css("display", "none");}
        if(data.price_gps == ""){priceGpsError.css("display", "block");_hasError++;}else{priceGpsError.css("display", "none");}
        if(data.price_plataform == "" ){pricePlatError.css("display", "block");_hasError++;}else{pricePlatError.css("display", "none");}
        if(data.rent_expiration == ""){rentExpiPlatError.css("display", "block");_hasError++;}else{rentExpiPlatError.css("display", "none");}
        if(data.purchase_order == ""){purchaseOrderError.css("display", "block");_hasError++;}else{purchaseOrderError.css("display", "none");}
        return _hasError;
}
/*
|--------------------------------------------------------------------------
| CREATE CLIENT
|--------------------------------------------------------------------------
*/
function createClient(){
    modalDialog.addClass("modal-lg");
    var form_client = createTemplate();
    modalBody.append(form_client);
    modal.modal('show');
}
modal.on('click', '#btn_create_client', function(e){
    e.preventDefault();
    var form = $('#create_client');
    var data = getFormData(form);
    const validate = validateFormClient(data);
    if(validate == 0){ 
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/createClient',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            timeout:5000,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $("#btn_create_client").html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 201){
                    tableClients.ajax.reload(null, false );
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right("Registro creado.", 'Notificación')}, 600);
                    setTimeout(function(){addContact(res.id);toastr_info_top_right("Añada contactos", 'Porfavor')}, 1200);     
                }else if(xhr.status == 204){
                    tableClients.ajax.reload(null, false );
                    modal.modal('hide');
                    setTimeout(function(){toastr_info_top_right("El cliente ya existe", 'Notificación')}, 600);  
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                if(errorThrown.status == 401 || errorThrown.status == 403){
                    modal.modal('hide');
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession(); 
                }else{
                    modal.modal('hide');
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $("#btn_create_client").html('Crear');
            }
        });
    }else{
        toastr_info_top_right("Porfavor rellene todos los campos.", 'Incompleto');
    }
});
/*
|--------------------------------------------------------------------------
| UPDATE CLIENT
|--------------------------------------------------------------------------
*/
function editClient(dataClient){
    modalDialog.addClass("modal-lg");
    var form_client = editTemplate(dataClient);
    modalBody.append(form_client);
    modal.modal('show');
}
modal.on('click', '#btn_edit_client', function(e){
    e.preventDefault();
    var form = $('#edit_client');
    var data = getFormData(form);
    const validate = validateFormClient(data);
    if(validate == 0){ 
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/editClient',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            timeout:5000,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $("#btn_edit_client").html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 200){
                    tableClients.ajax.reload(null, false );
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right("Registro actualizado.", 'Notificación')}, 600);    
                }else if(xhr.status == 204){
                    modal.modal('hide');
                    setTimeout(function(){toastr_info_top_right("Sin datos para actualizar.", 'Notificación')}, 600);
                    tableClients.ajax.reload(null, false); 
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                if(responseData.status == 401){
                    modal.modal('hide');
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession(); 
                }else{
                    modal.modal('hide');
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $("#btn_edit_client").html('Cambios guardados.');
            }
        });
    }else{
        toastr_info_top_right("Porfavor rellene todos los campos.", 'Incompleto');
    }
});
/*
|--------------------------------------------------------------------------
| DELETE CLIENT
|--------------------------------------------------------------------------
*/
function deleteClient(id, button){
    let data = {"id": id}
    let sess = sessionStorage.getItem("session")
        data.session = sess
    $.ajax({
        url: HOST_NAME+'/deleteClient',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        crossDomain: true,
        timeout:5000,
        beforeSend: function() {
            $(button).html(loading);
        },
        success: function(res, status ,xhr){
            if(xhr.status == 200){
                tableClients.ajax.reload(null, false );
                setTimeout(function(){toastr_success_top_right("Registro eliminado.", 'Notificación')}, 300);
            }else if(xhr.status == 204){
                modal.modal('hide');
                setTimeout(function(){toastr_info_top_right("Sin datos para actualizar.", 'Notificación')}, 300);
                tableClients.ajax.reload(null, false); 
            }
        },
        error: function (responseData, textStatus, errorThrown) {
            if(responseData.status == 401){
                modal.modal('hide');
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession(); 
            }else{
                modal.modal('hide');
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            }
        },
        complete: function() {
            $(button).html('<i class="fa fa-trash"></i>');
        }
    });

}
/*
|--------------------------------------------------------------------------
| HELPERS CONTACT
|--------------------------------------------------------------------------
*/
const validateEmail = function(email) {
    var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regexp.test(email);
}
const validateFormContact = function(data, action ='create'){
    let nameError = $("#nameError"), emailError = $("#emailError"),
        phoneError = $("#phoneError"), relError = $("#relError"), _hasError = 0;
    
        if(data.name == ""){
            nameError.css("display", "block");
            _hasError++;
        }else{
            nameError.css("display", "none");
        }
        if(data.phone == ""){
            phoneError.css("display", "block");
            _hasError++;
        }else{
            phoneError.css("display", "none");
        }
        if(!validateEmail(data.email)){
            emailError.css("display", "block");
            _hasError++;
        }else{
            emailError.css("display", "none");
        }
        if(data.relationship == ""){
            relError.css("display", "block");
            _hasError++;
        }else{
            relError.css("display", "none");
        }
        return _hasError;
}
/*
|--------------------------------------------------------------------------
| ADD CONTACTS TO CLIENT
|--------------------------------------------------------------------------
*/
function addContact(idClient){
    var form_contact = addContactTemplate(idClient);
    modalBody.append(form_contact);
    modal.modal('show');
}

modal.on('click', '#form_create_contact #btn_create_contact', function(e){
    e.preventDefault();
    var form = $('#form_create_contact');
    var data = getFormData(form);
    let validation = validateFormContact(data);
    if(validation == 0){
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/addContact',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            timeout:5000,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $('#btn_create_contact').html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 201){
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right("Registro creado.", 'Notificación')}, 700);    
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                if(responseData.status == 401){
                    modal.modal('hide');
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession(); 
                }else{
                    modal.modal('hide');
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $("#btn_create_contact").html('Add');
            }
        });
    }else{
        toastr_info_top_right("Porfavor rellene todos los campos", 'Incompleto');
    }

});
/*
|--------------------------------------------------------------------------
| SHOW CONTACTS
|--------------------------------------------------------------------------
*/
function showContacts(idClient, button){
    modalDialog.addClass("modal-lg");
    var tableContacts = showContactTemplate();
    let data = {"id_client": idClient}
    let sess = sessionStorage.getItem("session")
        data.session = sess
    $.ajax({
        url: HOST_NAME+'/getContactsByClient',
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        timeout:5000,
        beforeSend: function() {
            $(button).html(loading);
        },
        success: function(res, status ,xhr){
            if(xhr.status == 200){
                console.log(res);
                modalBody.append(tableContacts);
                var tbody = $(".tableContacts tbody"); 
                for(var i = 0 ; i < res.data.length ; i++){
                    tbody.append(showContactRowTemplate(res.data, i, idClient));
                }
                modal.modal("show");
            }else if(xhr.status == 204){
                toastr_info_top_right('Sin datos para mostrar','Notificación');
            }
        },
        error: function (responseData, textStatus, errorThrown) {
            if(responseData.status == 401){
                modal.modal('hide');
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession();   
            }else{
                modal.modal('hide');
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            }
        },
        complete: function() {
            $(button).html('<i class="fa fa-address-book"></i>');
        }
    });
}
//this block causes a loop if it is inside the function showContacts
modalBody.on("click", ".tableContacts #edit_contact", function(e){
    let idContact  = $(this).attr("data-id");   
    let dataRow = $(this).closest("tr").find("td").map(function() { return $(this).html();}).get();
    modal.modal('hide');
    editContact(idContact, dataRow[5], dataRow);
});
/*
|--------------------------------------------------------------------------
| UPDATE CONTACTS
|--------------------------------------------------------------------------
*/
function editContact(idContact, idClient, dataRow){
    var form_contact = editContactTemplate(dataRow, idContact, idClient);
    setTimeout(function(){
        modalBody.append(form_contact);
        modal.modal('show');
    },500);
}
//this block causes a loop if it is inside the function editContact
modal.on('click', '#form_edit_contact #btn_edit_contact', function(e){
    e.preventDefault();
    var form = $('#form_edit_contact');
    var data = getFormData(form);
    let validation = validateFormContact(data);
    if(validation == 0){
        data.id = data.id_contact;
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/editContact/',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            timeout:5000,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $('#btn_edit_contact').html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 200){
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right('Registro actualizado', 'Notificación')}, 700); 
                    setTimeout(function(){showContacts(res.id_client)}, 800);
                }else if(xhr.status == 204){
                    modal.modal('hide');
                    setTimeout(function(){toastr_info_top_right('Sin datos para actualizar.', 'Notificación')}, 700);
                    setTimeout(function(){showContacts(res.id_client)}, 800);    
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                if(responseData.status == 401){
                    modal.modal('hide');
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession();   
                }else{
                    modal.modal('hide');
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $('#btn_edit_contact').html('Edit');
            }
        });
    }else{
        toastr_info_top_right("Porfavor rellene todos los campos.", 'Incompleto');
    }
    
});

/*
|--------------------------------------------------------------------------
| DELETE CONTACT
|--------------------------------------------------------------------------
*/
modalBody.on("click", ".tableContacts #delete_contact", function(e){
    let idContact  = $(this).attr("data-id");  
    let row = $(this).parents("tr");
    modal.modal('hide');
    setTimeout(function(){
        swal({
            title: "Está seguro de eliminar este registro ?",
            text: "No podrá recuperar la información de este contacto!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                deleteContact(idContact, row);
            }
            else {
                swal.close();
                toastr_info_top_right('Acción abortada','Notificación');
            }
        });
    },500);
   
});
function deleteContact(idContact, row){
    let data = {"id": idContact}
    let sess = sessionStorage.getItem("session")
        data.session = sess
    $.ajax({
        url: HOST_NAME+'/deleteContact',
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        timeout:5000,
        contentType: 'application/json',
        data: JSON.stringify(data),
        beforeSend: function() {
            $('.showSweetAlert .confirm').html(loading);
        },
        success: function(res, status ,xhr){
            if(xhr.status == 200){
                swal.close();
                row.remove();
                setTimeout(function(){toastr_success_top_right("Registro eliminado.", 'Notificación')}, 400);
                setTimeout(function(){showContacts(res.id_client)}, 800);
            }else{
                swal.close();
                setTimeout(function(){toastr_info_top_right('Sin registros que eliminar.', 'Notificación')}, 400);
                setTimeout(function(){showContacts(res.id_client)}, 800);
            }
        },
        error: function (responseData, textStatus, errorThrown) {
            if(responseData.status == 401){
                swal.close();
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession(); 
            }else{
                swal.close();
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            }
            
        },
        complete: function() {
            $('.showSweetAlert .confirm').html("Eliminar");
        }
    });
}

/*
|--------------------------------------------------------------------------
| SHOW VEHICLES BY CLIENT
|--------------------------------------------------------------------------
*/
function showVehicles(idClient, button = null){
    $(button).html(loading);
    modalDialog.addClass("modal-lg");
    var tableVechicles = showVehicleTemplate();
    modalBody.append(tableVechicles);
    setTimeout(function(){
        initVehiclesByClientTable(idClient);
        modal.modal("show");
        $(button).html('<i class="fa fa-bus"></i>');
    },250);
}
/*
|--------------------------------------------------------------------------
| EDIT VEHICLES 
|--------------------------------------------------------------------------
*/
function editVehicle(dataRow){
    $.ajax({
        url: HOST_NAME+'/getAgreementsByClient',
        type: 'POST',
        dataType: 'json',
        crossDomain: true,
        timeout:5000,
        contentType: 'application/json',
        data: JSON.stringify({'id_client': dataRow.client_id, 'session': sessionStorage.getItem('session')}),
        success: function(res, status ,xhr){
            console.log(res)
            if(xhr.status == 200){
                const form_vehicle = editVehicleTemplate(dataRow, res);
                modal.modal('hide');
                setTimeout(function(){
                    modalBody.append(form_vehicle);
                    modal.modal('show');
                },600);
            }
        },
        error: function (responseData, textStatus, errorThrown) {
            console.log(responseData);
            if(responseData.status == 401){
                modal.modal('hide');
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession();
            }else{
                modal.modal('hide');
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            }
        }
    });
}

modalBody.on("click", "#form_edit_vehicle #btn_edit_vehicle", function(e){
    e.preventDefault();
    var form = $('#form_edit_vehicle');
    var data = getFormData(form);
    let sess = sessionStorage.getItem("session")
    data.session = sess
    $.ajax({
        url: HOST_NAME+'/updateVehicle',
        type: 'POST',
        dataType: 'json',
        crossDomain: true,
        timeout:5000,
        contentType: 'application/json',
        data: JSON.stringify(data),
        beforeSend: function() {
            $("#btn_edit_vehicle").html(loading);
        },
        success: function(res, status ,xhr){
            console.log(xhr);
            if(xhr.status == 200){
                modal.modal('hide');
                setTimeout(function(){toastr_success_top_right('Registro actualizado', 'Notificación')}, 700); 
                setTimeout(function(){showVehicles(res.client_id)}, 800);
            }else if(xhr.status == 204){
                modal.modal('hide');
                setTimeout(function(){toastr_info_top_right('Sin datos para actualizar.', 'Notificación')}, 700);
                setTimeout(function(){showVehicles(res.client_id)}, 800);
            }
        },
        error: function (responseData, textStatus, errorThrown) {
            if(responseData.status == 401){
                modal.modal('hide');
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession();
            }else{
                modal.modal('hide');
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            }
        },
        complete: function() {
            $('#btn_edit_vehicle').html('Editar');
        }
    });
});

/*
|--------------------------------------------------------------------------
| DELETE VEHICLES 
|--------------------------------------------------------------------------
*/
function deleteVehicle(idVehicle){
    let data = {"id": idVehicle}
    let sess = sessionStorage.getItem("session")
        data.session = sess
    $.ajax({
        url: HOST_NAME+'/delVehicle',
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        timeout:5000,
        contentType: 'application/json',
        data: JSON.stringify(data),
        beforeSend: function() {
            $('.showSweetAlert .confirm').html(loading);
        },
        success: function(res, status ,xhr){
            if(xhr.status == 200){
                setTimeout(function(){toastr_success_top_right("Registro eliminado.", 'Notificación')}, 400);
                setTimeout(function(){showVehicles(res.client_id)}, 800);
            }else{
                setTimeout(function(){toastr_info_top_right('Sin registros que eliminar.', 'Notificación')}, 400);
                setTimeout(function(){showVehicles(res.client_id)}, 800);
            }
            swal.close();
        },
        error: function (responseData, textStatus, errorThrown) {
            swal.close();
            if(responseData.status == 401){
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession(); 
            }else{
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            } 
        },
        complete: function() {
            $('.showSweetAlert .confirm').html("Eliminar");
        }
    });
}
/*
|--------------------------------------------------------------------------
| HELPERS AGREEMENTS
|--------------------------------------------------------------------------
*/
const validateAgreementForm = function(data){
    var groupError = $("#groupError"), startDateError = $("#startDateError"), dueDateError = $("#dueDateError"), hasError = 0;

    if(data.vehicle_group == ""){ groupError.css("display", "block");hasError++ }else{groupError.css("display", "none")}
    if(data.start_date == ""){startDateError.css("display", "block");hasError++ }else{startDateError.css("display", "none")}
    if(data.due_date == ""){dueDateError.css("display", "block");hasError++ }else{dueDateError.css("display", "none")}
    return hasError;
}
/*
|--------------------------------------------------------------------------
| ADD AGREEMENTS TO CLIENT 
|--------------------------------------------------------------------------
*/
function addAgreements(idClient){
    var form_agreement = addAgreementTemplate(idClient);
    modalBody.append(form_agreement);
    modal.modal('show');
}
modalBody.on("click", "#btn_create_agreement", function(e){
    e.preventDefault();
    var form = $('#form_create_agreement');
    var data = getFormData(form);
    const validate = validateAgreementForm(data);
    if(validate == 0){ 
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/createAgreement',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            timeout:5000,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $("#btn_create_agreement").html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 201){
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right("Contrato creado.", 'Notificación')}, 600);   
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                if(errorThrown.status == 401){
                    modal.modal('hide');
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession(); 
                }else{
                    modal.modal('hide');
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $("#btn_create_agreement").html('Crear');
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| SHOW AGREEMENTS BY CLIENT 
|--------------------------------------------------------------------------
*/
function showAgreements(idClient, button = null){
    $(button).html(loading);
    modalDialog.addClass("modal-lg");
    var tableAgrements = showAgreementTemplate();
    modalBody.append(tableAgrements);
    setTimeout(function(){
        initAgreementsbyClientTable(idClient);
        modal.modal("show");
        $(button).html('<i class="fa fa-handshake-o"></i>');
    },250);
}

/*
|--------------------------------------------------------------------------
| EDIT AGREEMENTS 
|--------------------------------------------------------------------------
*/
function editAgreement(data){
    const form_agreement_edit = editAgreementTemplate(data);
    modal.modal('hide');
    setTimeout(function(){
        modalBody.append(form_agreement_edit);
        modal.modal('show');
    },600);
}

modalBody.on("click", "#btn_edit_agreement", function(e){
    e.preventDefault();
    var form = $('#form_edit_agreement');
    var data = getFormData(form);
    const validate = validateAgreementForm(data);
    if(validate == 0){
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/updateAgreement',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            timeout:5000,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $("#btn_edit_agreement").html(loading);
            },
            success: function(res, status ,xhr){
                console.log(xhr);
                if(xhr.status == 200){
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right('Registro actualizado', 'Notificación')}, 700); 
                    setTimeout(function(){showAgreements(res.client_id)}, 800);
                }else if(xhr.status == 204){
                    modal.modal('hide');
                    setTimeout(function(){toastr_info_top_right('Sin datos para actualizar.', 'Notificación')}, 700);
                    setTimeout(function(){showAgreements(res.client_id)}, 800);
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                if(responseData.status == 401){
                    modal.modal('hide');
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession();
                }else{
                    modal.modal('hide');
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $('#btn_edit_agreement').html('Editar');
            }
        });
    }
});


function deleteAgreement(idAgreement){
    let data = {"id": idAgreement}
    let sess = sessionStorage.getItem("session")
        data.session = sess
    $.ajax({
        url: HOST_NAME+'/deleteAgreement',
        type: 'POST',
        crossDomain: true,
        dataType: 'json',
        timeout:5000,
        contentType: 'application/json',
        data: JSON.stringify(data),
        beforeSend: function() {
            $('.showSweetAlert .confirm').html(loading);
        },
        success: function(res, status ,xhr){
            if(xhr.status == 200){
                setTimeout(function(){toastr_success_top_right("Registro eliminado.", 'Notificación')}, 400);
                setTimeout(function(){showAgreements(res.client_id)}, 800);
            }else{
                setTimeout(function(){toastr_info_top_right('Sin registros que eliminar.', 'Notificación')}, 400);
                setTimeout(function(){showAgreements(res.client_id)}, 800);
            }
            swal.close();
        },
        error: function (responseData, textStatus, errorThrown) {
            swal.close();
            if(responseData.status == 401){
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession(); 
            }else{
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            } 
        },
        complete: function() {
            $('.showSweetAlert .confirm').html("Eliminar");
        }
    });
}