    let is_expired =  sessionStorage.getItem("expired");
    if(is_expired){ 
        alert("Por favor inicie session");
        sessionStorage.removeItem("expired");
    } 
    const validate_email = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    const validateFormLogin = function(){
        var email = $("#email").val(),
        pass  = $("#password").val(),
        hasError = 0;
        if(validate_email(email)){$("#emailError").css('visibility', 'hidden');}else{$("#emailError").css('visibility', 'visible'); hasError++;}
        if(pass){$("#passemptylError").css("display", "none");}else{$("#passemptylError").css("display", "block");hasError++;}
        return {hasError : hasError, email: email, pass: pass};
    }
    $("#login_submit").on("click", function(e){
        e.preventDefault();
        const validate = validateFormLogin();
        if(validate.hasError == 0){
            var data = {'email': validate.email, 'password': validate.pass};
            $.ajax({
                url: HOST_NAME+'/login',
                type: 'POST',
                crossDomain: true,
                data: data,
                success: function(res, status ,xhr){
                    console.log(xhr);
                    if(xhr.status === 201){
                        sessionStorage.setItem("session", JSON.parse(xhr.responseText));
                        window.location.replace("/pages/dashboard.html")
                    }
                },
                error: function (responseData, textStatus, errorThrown) {
                    alert("Credenciales incorrectos");
                }
            });
        }
    });
