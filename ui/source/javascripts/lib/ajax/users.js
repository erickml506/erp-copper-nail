/*
|--------------------------------------------------------------------------
| GLOBAL
|--------------------------------------------------------------------------
*/
var modal =  $('#modalDashboard'),
    modalBody  = $('#modalDashboard .modal-body'),
    loading = '<i class="fa fa-spinner fa-pulse fa-fw"></i>';
/*
|--------------------------------------------------------------------------
| HELPERS
|--------------------------------------------------------------------------
*/
const getFormData = function(form){
    var unindexed_array = form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
};
const validateEmail = function(email) {
    var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regexp.test(email);
}
const validateForm = function(data, action = 'create'){
    let nameError = $("#nameError"), lastError = $("#lastError"), 
        emailError = $("#emailError"), passError = $("#passError"),
        repassError = $("#repassError"), _hasError = 0; 
    if(data.name == ""){
        nameError.css("display", "block");
        _hasError++;
    }else{
        nameError.css("display", "none");
    }
    if(data.apellidos == ""){
        lastError.css("display", "block");
        _hasError++;
    }else{
        lastError.css("display", "none");
    }
    if(!validateEmail(data.email)){
        emailError.css("display", "block");
        _hasError++;
    }else{
        emailError.css("display", "none");
    }
    if(action == 'create'){
        if(data.password == ""){
            passError.css("display", "block");
            _hasError++;
        }else{
            passError.css("display", "none");
        }
        if(data.repassword !== data.password){
            repassError.css("display", "block");
            _hasError++;
        }else{
            repassError.css("display", "none");
        }
    }else if(action == 'edit'){
        if(data.password !== ""){
            if(data.password !== data.repassword){
                repassError.css("display", "block");
                _hasError++;
            }else{
                repassError.css("display", "none");
            }
        }
    }
    return _hasError;
}
/*
|--------------------------------------------------------------------------
| CREATE USER
|--------------------------------------------------------------------------
*/
function createUser(){
    var form_user = createTemplate();
    modalBody.append(form_user);
    modal.modal('show');
}
modal.on('click', '#btn_create_user', function(e){
    e.preventDefault();
    var form = $('#create_user');
    var data = getFormData(form);
    const validate = validateForm(data);
    if(validate == 0){
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/createUser',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $("#btn_create_user").html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 201){
                    tableUsers.ajax.reload(null, false );
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right("Registro creado.", 'Notificación')}, 700);
                }else if(xhr.status == 204){
                    tableUsers.ajax.reload(null, false );
                    modal.modal('hide');
                    setTimeout(function(){toastr_info_top_right("El usuario ya existe.", 'Notificación')}, 700);  
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                modal.modal('hide');
                if(responseData.status == 401 || responseData.status == 403){
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession();   
                }else{
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $("#btn_create_user").html('Create');
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| UPDATE USERS
|--------------------------------------------------------------------------
*/
function editUser(dataUser){
    var form_user = editTemplate(dataUser);
    modalBody.append(form_user);
    modal.modal('show');
}
modal.on('click', '#btn_edit_user', function(e){
    e.preventDefault();
    var form = $('#edit_user');
    var data = getFormData(form);
    const validate = validateForm(data, 'edit');
    if(validate == 0){
        let sess = sessionStorage.getItem("session")
        data.session = sess
        $.ajax({
            url: HOST_NAME+'/editUser',
            type: 'POST',
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function() {
                $("#btn_edit_user").html(loading);
            },
            success: function(res, status ,xhr){
                if(xhr.status == 200){
                    tableUsers.ajax.reload(null, false );
                    modal.modal('hide');
                    setTimeout(function(){toastr_success_top_right("Registro actualizado.", 'Actualización')}, 700);    
                }else if(xhr.status == 204){
                    modal.modal('hide');
                    setTimeout(function(){toastr_info_top_right("Sin datos para actualizar.", 'Notificación')}, 700);
                    tableUsers.ajax.reload(null, false); 
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                modal.modal('hide');
                if(responseData.status == 401 || responseData.status == 403){
                    toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                    updateSession(); 
                }else{
                    setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
                }
            },
            complete: function() {
                $("#btn_create_user").html('Cambios guardados.');
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| DELETE CLIENTS
|--------------------------------------------------------------------------
*/
function deleteUser(id, button){
    let data = {"id": id}
    let sess = sessionStorage.getItem("session")
        data.session = sess
    $.ajax({
        url: HOST_NAME+'/deleteUser',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        crossDomain: true,
        beforeSend: function() {
            $(button).html(loading);
        },
        success: function(res, status ,xhr){
            if(xhr.status == 200){
                tableUsers.ajax.reload(null, false );
                setTimeout(function(){toastr_success_top_right('Registro eliminado.', 'Eliminación')}, 300);
            }else if(xhr.status == 204){
                tableUsers.ajax.reload(null, false );
                setTimeout(function(){toastr_info_top_right("Sin datos para actualizar.", 'Notificación')}, 300);
            }
        },
        error: function (responseData, textStatus, errorThrown) {
            modal.modal('hide');
            if(responseData.status == 401 || responseData.status == 403){
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                updateSession(); 
            }else{
                setTimeout(function(){toastr_danger_top_right("Sucedió un error al procesar su solicitud.", 'Error')}, 200);
            }
        },
        complete: function() {
            $(button).html('<i class="fa fa-trash"></i>');
        }
    });

}