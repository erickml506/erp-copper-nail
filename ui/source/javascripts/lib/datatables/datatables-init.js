const language = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún datos disponible en la tabla",
    "sInfo":           "Mostrando del _START_ al _END_ de  _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrados de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
/*
|--------------------------------------------------------------------------
| LOAD CLIENTS
|--------------------------------------------------------------------------
*/
function format ( d ) {
    return '<strong>Dirección Fiscal:</strong>'+d.fiscal_address+' || '+'<strong> Dirección de instalación:</strong> '+d.install_address+'<br>'+
        '<strong>Hora de instalación:</strong> '+d.install_hours+'<br>'+ '<strong> Tipo de servicio: </strong>' + d.service_type+ '<br>'+
        '<strong>Número de vehículos:</strong> '+d.number_vehicles+' || '+' <strong>Payment Method:</strong> '+d.payment_method+'<br>'+ 
        '<strong>Precio GPS:</strong> '+d.price_gps+' || '+' <strong>Precio plataforma:</strong> '+d.price_plataform+' || '+' <strong>Rented:</strong> '+d.is_rented+'<br>'+
        '<strong>Usuario</strong> : '+d.user+' || '+' <strong>Clave:</strong> '+d.password+' || '+' <strong>Vencimiento de alquiler:</strong> '+d.rent_expiration+'<br>'+
        '<strong>Orden de compra:</strong> '+d.purchase_order+' '+' <strong>Numero de Operacion:</strong> '+ d.operation_number;
        
}
$.fn.dataTable.ext.legacy.ajax = false;
$.fn.dataTable.ext.errMode = 'throw';
var tableClients = $('#clients_table')
    .on('xhr.dt', function (e, settings, json, xhr) {
        if(xhr.status == 401 || xhr.status == 403){
            toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
            sessionStorage.setItem("refreshPage", true);
            updateSession();
        }else if(xhr.status == 500){
            toastr_info_top_right("Error del servidor, porfavor contacte al administrador", 'Error');
        }
    })
    .DataTable({
        dom: 'Bfrtip',
        pageLength: 10,
        deferRender: true,
        buttons: [
            {
                text: '<i class="fa fa-user-plus" aria-hidden="true"></i>',
                titleAttr: 'Agregar Cliente',
                className: 'createClient',
                action: function ( e, dt, node, config ) {
                    createClient();
                }
            },
            { extend: 'csv'},
            { extend: 'excel'},
            { extend: 'pdf'},
            { extend: 'print'}

        ],
        responsive: true,
        language: language,
        ajax: {
            url : HOST_NAME+'/allClients',
            type: 'POST',
            data: {'session': sessionStorage.getItem('session')},
            dataType: "json",
            dataSrc: function(json){
                json.data.map(function(element, i){
                    element.password = GibberishAES.dec(element.password, 'kyO7v2mihIl18Q8RA6hsj3kf3arPPQgKhvYV2jy9hVKrHzyxGOeg8Tjq7eh1');
                    element.rent_expiration =  element.rent_expiration.split("-").reverse().join('-');
                    element.is_rented = element.is_rented == 1 ? "Si" : "No";
                    element.service_type = element.service_type == '' ? '--' : element.service_type;
                });
                return json.data;
            }
        },
        columns:[
            {
                class:          "details-control",
                data:           null,
                defaultContent: ''
            },
            {"data": 'id'},
            {"data": 'type'},
            {"data": 'document'},
            {"data": 'business_name'},
            {
                class: 'text-left',
                data: 'Acción'
            }
        ],
        columnDefs: [{
            "targets": -1,
            "data": null,
            "defaultContent": ` <button id="add_agreements" class="btn btn-default" title="Agregar contrato"><i class="fa fa-handshake-o"></i></button>
                                <button id="show_agreements" class="btn btn-secondary" title="Mostrar contrato"><i class="fa fa-handshake-o"></i></button>
                                <button id="show_vehicle" class="btn btn-dark" title="Mostrar vehículos" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-bus"></i></button>
                                <button id="add_contact" class="btn btn-success" title="Añadir Contacto"><i class="fa fa-address-book-o"></i></button>
                                <button id="show_contacts" class="btn btn-warning" title="Mostrar contactos" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-address-book"></i></button>
                                <button id="edit" class="btn btn-info" title="Edit"><i class="fa fa-pencil-square-o"></i></button>
                                <button id="delete" class="btn btn-danger" title="Eliminar" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-trash"></i></button> `
        }]
    });
// Array to track the ids of the details displayed rows
var detailRows = [];
$('#clients_table tbody').on( 'click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableClients.row( tr );
    var idx = $.inArray( tr.attr('id'), detailRows );

    if ( row.child.isShown() ) {
        tr.removeClass( 'details' );
        row.child.hide();

        // Remove from the 'open' array
        detailRows.splice( idx, 1 );
    }
    else {
        tr.addClass( 'details' );
        row.child( format( row.data() ) ).show();

        // Add to the 'open' array
        if ( idx === -1 ) {
            detailRows.push( tr.attr('id') );
        }
    }
});
// On each draw, loop over the `detailRows` array and show any child rows
tableClients.on( 'draw', function () {
    $.each( detailRows, function ( i, id ) {
        $('#'+id+' td.details-control').trigger( 'click' );
    });
});
// click action buttons
$('#clients_table tbody').on( 'click', 'button', function () {
    let action = $(this).attr("id");
    var dataClient = tableClients.row($(this).parents('tr')).data();
    var button = $(this);
    switch (action) {
        case 'add_agreements':
            addAgreements(dataClient.id)
            break;
        case 'show_agreements':
            showAgreements(dataClient.id, button)
            break;
        case 'show_vehicle':
            showVehicles(dataClient.id, button);
            break;
        case 'edit':
            editClient(dataClient);
            break;
        case 'show_contacts':
            showContacts(dataClient.id, button);
            break;
        case 'add_contact':
            addContact(dataClient.id);
            break;
        case 'delete':
            swal({
                title: "Está seguro de eliminar este registro ?",
                text: "No podrá recuperar la información de este cliente!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    swal.close()
                    deleteClient(dataClient.id, button);
                }
                else {
                    swal.close()
                    toastr_info_top_right('Acción abortada','Cancelado');
                }
            });
    }
});

/*
|--------------------------------------------------------------------------
| LOAD USERS
|--------------------------------------------------------------------------
*/
var tableUsers = $('#users_table')
    .on('xhr.dt', function (e, settings, json, xhr) {
        if(xhr.status == 401 || xhr.status == 403){
            toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
            sessionStorage.setItem("refreshPage", true);
            updateSession();
        }
    })
    .DataTable({
        dom: 'Bfrtip',
        //serverSide: true,
        pageLength: 10,
        //length: 15,
        deferRender: true,
        buttons: [
            {
                text: '<i class="fa fa-user-plus" aria-hidden="true"></i>',
                titleAttr: 'Agregar usuario',
                className: 'createUser',
                action: function ( e, dt, node, config ) {
                    createUser();
                }
            },
            { extend: 'csv'},
            { extend: 'excel'},
            { extend: 'pdf'},
            { extend: 'print'}

        ],
        responsive: true,
        language: language,
        ajax: {
            url: HOST_NAME+'/allUsers',
            type: 'POST',
            data: {'session': sessionStorage.getItem('session')},
            dataType: "json"
        },
        columns:[
            {data: 'id'},
            {data: 'name'},
            {data: 'apellidos'},
            {data: 'email'},
            {data: 'type'},
            {
                class: 'text-left',
                data: 'Action'
            }
        ],
        columnDefs: [{
            "targets": -1,
            "data": null,
            "defaultContent": `<button id="delete" class="btn btn-danger" title="Delete" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-trash"></i></button>
                            <button id="edit" class="btn btn-info" title="Modificar"><i class="fa fa-pencil-square-o"></i></button>`
        }]
    });

$('#users_table tbody').on( 'click', 'button', function () {
    let action = $(this).attr("id");
    var dataUser = tableUsers.row($(this).parents('tr')).data();
    var button = $(this);
    switch (action) {
        case 'edit':
            editUser(dataUser);
            break;
        case 'delete':
            swal({
                title: "Está seguro de eliminar este registro ?",
                text: "No podrá recuperar la información de este cliente!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    swal.close()
                    deleteUser(dataUser.id, button);
                }
                else {
                    swal.close()
                    toastr_info_top_right('Acción abortada','Cancelado');
                }
            });
    }
});


/*
|--------------------------------------------------------------------------
| LOAD VEHICLES BY CLIENT
|--------------------------------------------------------------------------
*/
function initVehiclesByClientTable(idClient){
    const  format_ = function ( d ) {
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                    '<tr>'+
                        '<td><strong>Modelo:</strong></td>'+
                        '<td>'+d.model+'</td>'+
                        '<td><strong>Placa:</strong></td>'+
                        '<td>'+d.plate+'</td>'+
                        '<td><strong>Brand:</strong></td>'+
                         '<td>'+d.brand+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td><strong>Teléfono:</strong></td>'+
                        '<td>'+d.phone+'</td>'+
                        '<td><strong>Simcard:</strong></td>'+
                        '<td>'+d.simcard+'</td>'+
                        '<td><strong>Protocolo:</strong></td>'+
                        '<td>'+d.protocol+'</td>'+
                    '</tr>'+
                '</table>';
    }
    var tableVehicles = $('#vehicles_table')
        .on('xhr.dt', function (e, settings, json, xhr) {
            if(xhr.status == 401 || xhr.status == 403){
                toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
                sessionStorage.setItem("refreshPage", true);
                updateSession();
            }
        })
        .DataTable({
            dom: 'Bfrtip',
            //serverSide: true,
            pageLength: 5,
            deferRender: true,
            buttons: [
                { extend: 'csv'},
                { extend: 'excel'},
                { extend: 'pdf'},
                { extend: 'print'}
        
            ],
            responsive: true,
            language: language,
            ajax: {
                url : HOST_NAME+'/getVehiclesByClient',
                type: 'POST',
                data: {'id': idClient, 'session': sessionStorage.getItem('session')},
                dataType: "json",
                dataSrc: function(json){
                    if(json.data.length > 0){
                        return json.data;
                    }else{
                        toastr_info_top_right('Sin datos para mostrar.', 'Notificación')
                        return json;     
                    }
                }
            },
            columns:[
                {
                    class:          "details-control",
                    data:           null,
                    defaultContent: ''
                },
                {data: 'imei'},
                {data: 'name'},
                {data: 'group'},
                {
                    class: 'text-left',
                    data: 'Action'
                }
            ],
            columnDefs: [{
                "targets": -1,
                "data": null,
                "defaultContent": `<button id="delete" class="btn btn-danger" title="Eliminar" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-trash"></i></button>
                                <button id="edit" class="btn btn-info" title="Modificar"><i class="fa fa-pencil-square-o"></i></button>`
            }]
        });
    var detailRows = [];
    $('#vehicles_table tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = tableVehicles.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();

            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format_( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    });
    // On each draw, loop over the `detailRows` array and show any child rows
    tableVehicles.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        });
    });

    $('#vehicles_table tbody').on( 'click', 'button', function () {
        let action = $(this).attr("id");
        var dataVehicle = tableVehicles.row($(this).parents('tr')).data();
        var button = $(this);
        switch (action) {
            case 'edit':
                editVehicle(dataVehicle);
                break;
            case 'delete':
                modal.modal('hide');
                setTimeout(function(){
                    swal({
                        title: "Está seguro de eliminar este registro ?",
                        text: "No podrá recuperar la información de este vehiculo!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Eliminar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            deleteVehicle(dataVehicle.id);
                        }
                        else {
                            swal.close()
                            showVehicles(dataVehicle.client_id)
                            toastr_info_top_right('Acción abortada','Cancelado');
                        }
                    });
                }, 500);
        }
    });
}

/*
|--------------------------------------------------------------------------
| LOAD AGREEMENTS
|--------------------------------------------------------------------------
*/

function initAgreementsbyClientTable(idClient){
    var tableAgreements = $('#agreements_table')
    .on('xhr.dt', function (e, settings, json, xhr) {
        if(xhr.status == 401 || xhr.status == 403){
            toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
            sessionStorage.setItem("refreshPage", true);
            updateSession();
        }
    })
    .DataTable({
        dom: 'Bfrtip',
        //serverSide: true,
        pageLength: 5,
        deferRender: true,
        buttons: [
            { extend: 'csv'},
            { extend: 'excel'},
            { extend: 'pdf'},
            { extend: 'print'}
    
        ],
        responsive: true,
        language: language,
        ajax: {
            url : HOST_NAME+'/getAgreementsByClient',
            type: 'POST',
            data: {'id_client': idClient, 'session': sessionStorage.getItem('session')},
            dataType: "json",
            dataSrc: function(json){
                if(json.data.length > 0){
                    return json.data;
                }else{
                    toastr_info_top_right('Sin datos para mostrar.', 'Notificación')
                    return json;     
                }
            }
        },
        columns:[
            {data: 'vehicle_group'},
            {data: 'start_date'},
            {data: 'due_date'},
            {
                class: 'text-left',
                data: 'Action'
            }
        ],
        columnDefs: [{
            "targets": -1,
            "data": null,
            "defaultContent": `<button id="delete" class="btn btn-danger" title="Eliminar" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-trash"></i></button>
                            <button id="edit" class="btn btn-info" title="Modificar"><i class="fa fa-pencil-square-o"></i></button>`
        }]
    });
    $('#agreements_table tbody').on( 'click', 'button', function () {
        let action = $(this).attr("id");
        var dataAgreement = tableAgreements.row($(this).parents('tr')).data();
        var button = $(this);
        switch (action) {
            case 'edit':
                editAgreement(dataAgreement);
                break;
            case 'delete':
                modal.modal('hide');
                setTimeout(function(){
                    swal({
                        title: "Está seguro de eliminar este registro ?",
                        text: "No podrá recuperar la información de este contrato!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Eliminar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            deleteAgreement(dataAgreement.id);
                        }
                        else {
                            swal.close()
                            showAgreements(dataAgreement.client_id)
                            toastr_info_top_right('Acción abortada','Cancelado');
                        }
                    });
                }, 500);
        }
    });
}

var tableAgreements = $('#charging_table')
    .on('xhr.dt', function (e, settings, json, xhr) {
        if(xhr.status == 401 || xhr.status == 403){
            toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
            sessionStorage.setItem("refreshPage", true);
            updateSession();
        }
    })
    .DataTable({
        dom: 'Bfrtip',
        //serverSide: true,
        pageLength: 5,
        deferRender: true,
        buttons: [
            { extend: 'csv'},
            { extend: 'excel'},
            { extend: 'pdf'},
            { extend: 'print'}
    
        ],
        responsive: true,
        language: language,
        ajax: {
            url : HOST_NAME+'/getCharging',
            type: 'POST',
            data: {'session': sessionStorage.getItem('session')},
            dataType: "json",
            dataSrc: function(json){
                if(json.data.length > 0){
                    return json.data;
                }else{
                    toastr_info_top_right('Sin datos para mostrar.', 'Notificación')
                    return json;     
                }
            }
        },
        columns:[
            {data: 'id'},
            {data: 'client_id'},
            {data: 'billing_date'},
            {data: 'amount'}
        ]
    });

 
   
