var modal =  $('#modalDashboard'),
    modalBody  = $('#modalDashboard .modal-body');

window._url_all 		= HOST_NAME+'/allBillings';
window._url_delete 		= HOST_NAME+'/delBilling';
window._url_delete_item = HOST_NAME+'/delBillingItem';
window._url_all_items 	= HOST_NAME+'/getBillingItems';
window._url_add_item 	= HOST_NAME+'/addBillingItem';
window._url_create 		= HOST_NAME+'/addBilling';
window._url_update 		= HOST_NAME+'/updateBilling';
window._url_find_client = HOST_NAME+'/findClient';
window._url_find_vehicle_by = HOST_NAME+'/findVehicleBy';
/*
window._url_api = "http://api.geointranet";

window._url_all 		= _url_api+'/v1/billing/all';
window._url_delete 		= _url_api+'/v1/billing/delete'; 
window._url_delete_item = _url_api+'/v1/billing/deleteItem';
window._url_all_items 	= _url_api+'/v1/billing/allItems';
window._url_add_item 	= _url_api+'/v1/billing/addItem';
window._url_create 		= _url_api+'/v1/billing/create';
window._url_update 		= _url_api+'/v1/billing/update';
window._url_find_client = _url_api+'/v1/client/findByName';
window._url_find_vehicle_by = _url_api+'/v1/vehicles/findBy';
*/

/*
window._url_api = "http://api.geointranet";
window._url_find_vehicle_by = _url_api+'/v1/vehicles/findBy';
*/


$(window).ready(function(evt){
	window._vehicles_set = {};
	window._table = $("#regs_table"); 
	window._items_table = $("#items_table"); 
	window._filter_order = 0;
	filter_list();  
	header_events();
	modal_events();

	$('#billing_date').datetimepicker({
        viewMode: 'days',
        format: 'DD/MM/YYYY'
    });
    $('#billing_date>input').click(function(evt){
    	$('#billing_date').data("DateTimePicker").show();
    });
    $('#add-reg').click(function(evt){
    	$('#modal-reg-details').attr("reg-id","").modal("show");
    	$("#items_table>tbody").html(
    		`<tr>
    			<td colspan='4'></td>
    		</tr>`
    	);
    	$(`
    		#modal-reg-details .modal-body input,
    		#modal-reg-details .modal-body select
    	`).val("");
    	$("#client_info_name").html("Sin Cliente");
    	$("#modal_select_client").html("<option value=''>Seleccionar</option>");
    	$("a.nav-link[href='#primary']").trigger("click");
    	$("a.nav-link[href='#secondary']").hide();
    });
    modal_find_client();
    $("#toggle_modal_find_client").click(function(evt){
    	$("#g_find_client").toggle();
    }); 
    $("#toggle-new-item").click(function(evt){
    	var _obj = $(this);
    	if(_obj.find("i").hasClass("fa-plus")){
    		_obj.find("i").removeClass("fa-plus").addClass("fa-minus");
    		
    	}else{
    		_obj.find("i").removeClass("fa-minus").addClass("fa-plus");
    	}
    	$('#add-reg-form').toggle();
    	if($("#add-reg-form").is(":visible")){
			$("#search-item-form").hide();
		}
    });
    $("#toggle-search-item").click(function(evt){
    	var _obj = $(this);
    	$("#search-item-form").toggle();
    	if($("#search-item-form").is(":visible")){
    		$('#add-reg-form').hide();
			$("#toggle-new-item>i").removeClass("fa-minus").addClass("fa-plus");
    	}
    });
    $('#add-reg-form').hide();
    $("#search-item-form").hide();
    $("#add-new-item-input").keyup(function(evt){
    	var _obj = $(this);
    	buscar_item_a_agregar(_obj.val());
    });
    $("#buscar_item_inside_billing").keyup(function(evt){
    	var _obj = $(this);
    	var _search = _obj.val() ? _obj.val() : "";
    	reload_billing_items($("#modal-reg-details").attr("reg-id"),_search);
    });
    $("#add-item-list").click(function(evt){  
    	add_item_to_billing_items(
    		$("#add-reg-items").val(),//select id vehicle
    		$("#add-reg-price").val()//define price
    	);
    }); 
    $("#clear_find_item").click(function(evt){  
    	$("#buscar_item_inside_billing").val("");
    	reload_billing_items($("#modal-reg-details").attr("reg-id"),'');
    }); 
    $("#cycle").change(function(evt){  
    	var obj = $(this);
    	if(obj.val()==20 || obj.val()==0){
    		$("#content-price").show();
    	}else{
    		$("#content-price").hide();
    	}
    }); 
});
function add_item_to_billing_items(vehicle_id,price){
	var _billing_id = $("#modal-reg-details").attr("reg-id");
	var _data = {
        	billing_id:_billing_id,
        	vehicles_id:vehicle_id,
        	price:price,
	    	session: sessionStorage.getItem('session'),
        };

   	console.log("addItem",_data);
	$.ajax({ 
        url: _url_add_item,
        type: 'POST', 
        dataType:'json',
        contentType:'application/json',
        data:JSON.stringify(_data), 
        success: function(res, status ,xhr){ 
        	console.log(res,status,xhr);  
        	reload_billing_items(_billing_id);
        	setTimeout(function(){toastr_success_top_right("Item agregado", 'Nuevo Item')}, 600);
        },
        error: function (responseData, textStatus, errorThrown) {
        	console.log(responseData,textStatus,errorThrown); 
        	setTimeout(function(){toastr_danger_top_right("No se pudo agregar el item", 'Nuevo Item')}, 600);
        },
        complete: function() {  
        }
    }); 
}
function reload_billing_items(billing_id,search = ''){
	window._search_item = search || '';
	$.ajax({ 
        url: _url_all_items,
        type: 'POST', 
        dataType:'json',
        contentType:'application/json',
        data:JSON.stringify({
        	billing_id:billing_id,
	  		session: sessionStorage.getItem('session'),
	  		search:search,
        }), 
        success: function(res, status ,xhr){ 
        	console.log(res,status,xhr); 
        	var _pass = false;
        	if(!window._items_set)window._items_set = {};
        	if(res.items && res.items.length>0){
        		_pass = true;
        		window._items_set[billing_id] = res.items;
        	}
        	if(res.vehicles && typeof res.vehicles == "object"){
        		for(var idx in res.vehicles){
        			var veh = res.vehicles[idx];
        			window._vehicles_set[veh.id] = veh;
        		}
        	}
        	window._vehicles_items_filter = res.vehicles_filter || {};
        	window._vehicles_items_filter_length = res.vehicles_filter_length || 0;
        	
        	if(_pass){
        		render_items_table(billing_id);
        		setTimeout(function(){toastr_info_top_right("Refrescando items de la tabla", 'Actualización')}, 600);
        	}
        },
        error: function (responseData, textStatus, errorThrown) {
        	console.log(responseData,textStatus,errorThrown); 
        },
        complete: function() {  
        }
    }); 
}
function buscar_item_a_agregar(text){
	$("#add-reg-items").html("<option value=''>Espere porfavor...</option>");
	$.ajax({ 
        url: _url_find_vehicle_by,
        type: 'POST', 
        dataType:'json',
        contentType:'application/json',
        data:JSON.stringify({
        	search:text,
	     	session: sessionStorage.getItem('session'),
        }), 
        success: function(res, status ,xhr){ 
        	console.log(res,status,xhr); 
        	var _str = `<option value=''>SELECCIONAR</option>`;
        	if(res && res.data && Array.isArray(res.data) && res.data.length>0){
        		for(var idx in res.data){
        			var _item = res.data[idx];
        			_str+=`<option value='${_item.id}'>${_item.plate} / ${_item.imei}</option>`;
        		}
        	}
        	$("#add-reg-items").html(_str);
        },
        error: function (responseData, textStatus, errorThrown) {
        	console.log(responseData,textStatus,errorThrown); 
        	$("#add-reg-items").html("<option value=''>Error...</option>");
        },
        complete: function() {  
        }
    }); 
}
function modal_find_client(){
	$("#modal_find_client").keyup(function(evt){
		var _obj = $(this);
		var _key = evt.which;
		console.log(_key);
		if(!_key)_key = evt.keyCode;
		console.log(_key);
		if(_key==13){
			var _data = { 
	        	name:_obj.val(),
	        	session: sessionStorage.getItem('session'),
	        }; 
			$.ajax({ 
		        url: _url_find_client,
		        type: 'POST',
		        //type: 'DELETE',
		        dataType:'json',
		        contentType:'application/json',
		        data:JSON.stringify(_data), 
		        success: function(res, status ,xhr){
		        	if(res.data && Array.isArray(res.data) && res.data.length>0){
		        		var _str = "<option value=''>Seleccionar</option>";
		        		for(var idx in res.data){
		        			_str+=`<option value='${res.data[idx].id}'>${res.data[idx].business_name}</option>`;
		        		}
		        		$("#modal_select_client").html(_str);
		        	}else{
		        		$("#modal_select_client").html(
		        			`<option>Sin registros</option>`
		        			);
		        	}
		        	console.log(res,status,xhr); 
		        },
		        error: function (responseData, textStatus, errorThrown) {
		        	console.log(responseData,textStatus,errorThrown); 
		        },
		        complete: function() {  
		        }
		    }); 
		}
	});
}
function transformBillingDate(val){
	console.log(val);
	if(typeof val!=="undefined" && typeof val!=="object"){
		val +="";
	}else val="";
	val = val.split("-");
	if(Array.isArray(val) && val.length>2){
		return `${val[2]}/${val[1]}/${val[0]}`; 
		//return `${val[0]}/${val[1]}`; 
	}else{
		return "00/00/00";
		//return "00/00";
	}
}
function header_events(){
	$("#regs_table.geo-api thead th:not(.disabled)").each(function(idx,obj){
		$(obj).addClass("fa geo-api-sort").click(function(evt){
			evt.preventDefault();
			if(window.prevent_filter_ajax)return;
			var _th_ref = this;
			var _ref = $(_th_ref);
			_table.attr("filter-key",_ref.attr("attr-key")); 
			if(_ref.hasClass("geo-api-sort")){
				_ref.removeClass("geo-api-sort").addClass("geo-api-sort-up");
				_filter_order = 1;
			}else if(_ref.hasClass("geo-api-sort-up")){
				_ref.removeClass("geo-api-sort-up").addClass("geo-api-sort-down");
				_filter_order = -1;
			}else{
				_ref.removeClass("geo-api-sort-down").addClass("geo-api-sort-up");
				_filter_order = 1;
			}
			$("#regs_table.geo-api thead th:not(.disabled)").each(function(ev){
				var _th = this;
				if(_th!=_th_ref){
					$(_th).removeClass("geo-api-sort-up geo-api-sort-down").addClass("geo-api-sort");
				}
			});
			filter_list();
		});
	});
	$("input[name=global_search]").keyup(function(evt){
		filter_list();
	});



	$("#items_table.geo-api thead th:not(.disabled)").each(function(idx,obj){
		$(obj).addClass("fa geo-api-sort").click(function(evt){
			evt.preventDefault();
			if(window.prevent_filter_item_ajax)return;
			var _th_ref = this;
			var _ref = $(_th_ref);
			_items_table.attr("filter-key",_ref.attr("attr-key")); 
			if(_ref.hasClass("geo-api-sort")){
				_ref.removeClass("geo-api-sort").addClass("geo-api-sort-up");
				_filter_order = 1;
			}else if(_ref.hasClass("geo-api-sort-up")){
				_ref.removeClass("geo-api-sort-up").addClass("geo-api-sort-down");
				_filter_order = -1;
			}else{
				_ref.removeClass("geo-api-sort-down").addClass("geo-api-sort-up");
				_filter_order = 1;
			}
			$("#items_table.geo-api thead th:not(.disabled)").each(function(ev){
				var _th = this;
				if(_th!=_th_ref){
					$(_th).removeClass("geo-api-sort-up geo-api-sort-down").addClass("geo-api-sort");
				}
			});
			filter_item_list();
		});
	});
	$("input[name=global_item_search]").keyup(function(evt){
		filter_item_list();
	});
}
function filter_list(){
	if(window.prevent_filter_ajax)return;
	window.prevent_filter_ajax = true;
	var _page = window._page_ref || "";
	var _data = {
		global_search:$("input[name=global_search]").val(),
		filter_order:_filter_order,
		filter_field:_table.attr("filter-key"),
		session: sessionStorage.getItem('session'),
		page:_page,
	}; 

	console.log("beforeFilter",_data);

	$.ajax({ 
        url: _url_all,
        type: 'POST', 
        dataType:'json',
	 	contentType: "application/json", 
        data:JSON.stringify(_data), 
        success: function(response, status ,xhr){
			console.log(response, status ,xhr);
        	if(window._reg_set)delete window._reg_set;
			window._reg_set = {};
			var _body = window._table.find("tbody").empty();
	 		var _str = "";
			if(response && response.data){ 
				var data = response.data;
				for(var idx in data){
					_reg_set[data[idx].id] = data[idx];
					_str +=
					`<tr>
						<td>${data[idx].id}</td>
						<td>${data[idx].code}</td>
						<td>${transformBillingDate(data[idx].billing_date)}</td>
						<td>${getDocType(data[idx].doc_type)}</td>
						<td>${getCycle(data[idx].cycle)}</td>
						<td>${getType(data[idx].type)}</td>
						<td>
							<div>
								<div reg-id="${data[idx].id}" class="btn btn-default geo-api-btn-del">
									<i class="fa fa-trash"></i>
								</div>
								<div reg-id="${data[idx].id}" class="btn btn-default geo-api-btn-edit">
									<i class="fa fa-edit"></i>
								</div>
							</div>
						</td>
					</tr>`;
				}
				_body.html(_str);
				$("#pag_table").html(getPagination(response.details));
				pagination_events();
				action_events();
			}else{
				_str =
				`<tr>
					<td colspan='7'>Registros no encontrados.</td> 
				</tr>`;
				_body.html(_str);
				$("#pag_table").html("");
			}
        	if(response.clients){
        		window._clients_set = response.clients;
        	}else{
        		if(window._clients_set)delete window._clients_set;
        		window._clients_set = {};
        	}
        	if(response.items){
        		window._items_set = response.items;
        	}else{
        		if(window._items_set)delete window._items_set;
        		window._items_set = {};
        	}
        	if(response.vehicles){
        		window._vehicles_set = response.vehicles;
        	}else{
        		if(window._vehicles_set)delete window._vehicles_set;
        		window._vehicles_set = {};
        	}
			window.prevent_filter_ajax = false;
        },
        error: function (responseData, textStatus, errorThrown) {
        	if(window._reg_set)delete window._reg_set; 
			console.log(responseData, textStatus, errorThrown);
			window.prevent_filter_ajax = false;
			if(responseData.status == 401){
				toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
				sessionStorage.setItem("refreshPage", true);
                updateSession(); 
            }
        },
        complete: function() {  
        }
    });  
}
function pagination_events(){
	var _nav = $("#nav-pag-table");
	_nav.find("li[page-ref]:not(.disabled)").click(function(evt){
		evt.preventDefault();
		var _li = this;
		window._page_ref = $(_li).attr("page-ref");
		filter_list();
	});
}
function action_events(){
	$(".geo-api-btn-del").click(function(evt){
		var _id = $(this).attr("reg-id");
		var _modal = $("#modal-reg-remove");
		_modal.find("[reg-id]").attr("reg-id",_id);
		_modal.modal("show");
	});
	$(".geo-api-btn-edit").click(function(evt){
		$("#g_find_client").hide();
		var _id = $(this).attr("reg-id");
		var _data = _reg_set[_id];
		$("a.nav-link[href='#secondary']").show();
		$("a.nav-link[href='#primary']").trigger("click");
		var _modal = $("#modal-reg-details");
		_modal.attr("reg-id",_id);
		for(var _idx in _data){
			if(_idx=="billing_date")continue;
			_modal.find(`input[name=${_idx}]`).val(_data[_idx]);
		} 

		var _client = _clients_set && _data.client_id in _clients_set ? _clients_set[_data.client_id] : "";
		console.log("client",_client,_data.client_id);
		$("#client_info_name").html(_client.business_name);

		render_items_table(_id);

		_modal.find("input[name=billing_date]").val(transformBillingDate(_data.billing_date));
		_modal.find("select[name=type]").val(_data.type);
		_modal.find("select[name=doc_type]").val(_data.doc_type);
		_modal.find("select[name=cycle]").val(_data.cycle); 
		_modal.find("input[name=price]").val(_data.price); 
		_modal.modal("show");
		if(_data.cycle==20 || _data.cycle==0){
			$("#content-price").show();
		}else{
			$("#content-price").hide();
		}
	});
}
function modal_events(){
	$("#del-reg").click(function(evt){
		var _id = $(this).attr("reg-id");
		var _data = {
	        	id:_id,
	        	session: sessionStorage.getItem('session'),
	        }; 
		$.ajax({ 
	        url: _url_delete,
	        type: 'POST',
	        //type: 'DELETE',
	        dataType:'json',
	        contentType:'application/json',
	        data:JSON.stringify(_data), 
	        success: function(res, status ,xhr){
	        	console.log(res,status,xhr);
	        	if(status=="success"){
	        		setTimeout(function(){toastr_success_top_right("Registro eliminado", 'Eliminación')}, 600);
	        	}else{
	        		setTimeout(function(){toastr_danger_top_right("No se pudo eliminar el registro seleccionado.", 'Notificación')}, 600);
	        	}
	        	filter_list();
	        },
	        error: function (responseData, textStatus, errorThrown) {
	        	console.log(responseData,textStatus,errorThrown);
						 setTimeout(function(){toastr_danger_top_right("Error al intentar eliminar el registro.", 'Error')}, 600);
				if(responseData.status == 401){
					toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
					updateSession(); 
				}
	        },
	        complete: function() { 
	        	var _modal = $("#modal-reg-remove");
	        	_modal.modal("hide");
	        }
	    }); 
	});
	$("#btn-update").click(function(evt){
    	var _modal = $("#modal-reg-details");
    	var _reg_id = _modal.attr("reg-id"); 
		swal({
            title: (is_numeric( _reg_id ) ? "Confirmar actualización ?" : "Confirmar nuevo registro"),
            text: (is_numeric( _reg_id ) ? "Los cambios serán guardados en el servidor." : "Se creará un nuevo registro de factura en el servidor."),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Proceder",
            cancelButtonText: "Rechazar",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm){
            console.log("isConfirm",isConfirm); 
            if(isConfirm){
            	var _modal = $("#modal-reg-details");
            	var _reg_id = _modal.attr("reg-id"); 
            	var _data = {
            		session: sessionStorage.getItem('session'),
            		id:_reg_id,
            	}; 
            	_modal.find(".modal-body input[persistent],.modal-body select[persistent]").each(function(idx,ipt){
            		_data[$(ipt).attr("name")] = $(ipt).val();
            	});
            	if(is_numeric( _reg_id )){
            		_data["client_id"] = _reg_set[_reg_id].client_id;
            	}else{
            		_data["client_id"] = $("#modal_select_client").val();
            	} 
            	console.log("beforeUpdate",_data);
            	if(!$("#g_find_client").is(":visible")){
            		_data["new_client_id"] = null;
            	} 
				$.ajax({ 
			        url: (is_numeric( _reg_id ) ? _url_update : _url_create),
			        type: (is_numeric( _reg_id ) ? 'POST'/*PUT*/ : 'POST'),
			        //type: 'PUT',
			        contentType: "application/json", 
			        crossDomain: true, 
			        data:JSON.stringify(_data),
	        		dataType:'json',
			        success: function(res, status ,xhr){
			        	console.log(res,status,xhr);
			        	if(status=="success"){
			        		if(is_numeric( _reg_id )){
			        			setTimeout(function(){toastr_success_top_right("Registro actualizado.", 'Actualización')}, 600);
			        		}else{
			        			_reg_set[res.id] = res; 
		        				$("#client_info_name").html(
		        					$(`#modal_select_client option[value=${$("#modal_select_client").val()}]`).html()
		        				); 
		        				$("a.nav-link[href='#secondary']").show();
		        				$("#modal-reg-details").attr("reg-id",res.id);
			        			setTimeout(function(){toastr_success_top_right("Registro creado.", 'Notificación')}, 600);
			        		}
			        		
			        	}else{
			        		if(is_numeric( _reg_id )){
			        			setTimeout(function(){toastr_danger_top_right("Cambios no actualizados.", 'Actualización')}, 600);
			        		}else{
			        			setTimeout(function(){toastr_danger_top_right("No se pudo crear un nuevo registro.", 'Notificación')}, 600);
			        		}
			        		
			        	}
	        			filter_list();
			        },
			        error: function (responseData, textStatus, errorThrown) {
			        	console.log(responseData,textStatus,errorThrown);
			        	if(is_numeric( _reg_id )){
			        		setTimeout(function(){toastr_danger_top_right("Error al registrar los cambios.", 'Error')}, 600);
			        	}else{
			        		setTimeout(function(){toastr_danger_top_right("Error al intentar crear un nuevo registro.", 'Error')}, 600);
			        	} 
						if(responseData.status == 401){ 
							toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
							updateSession(); 
						}
			        },
			        complete: function() { 
			        }
			    }); 
            }
        });
	});  
}
function render_items_table(billing_id){
	$("#items_table").attr("billing-id",billing_id);
	var items_table = $("#items_table>tbody");
	var _items = window._items_set && billing_id in _items_set ? _items_set[billing_id] : [];
	
	if(window._search_item && _search_item.length>0 && _vehicles_items_filter_length==0){

		items_table.html("<tr><td colspan='4'>Sin registros...</td></tr>");
		return;
	}

	if(_items.length>0){
		var _count = 0;
		var _str = "";
		for(var idx in _items){
			var _veh_id = _items[idx].vehicles_id;
			if(
				window._vehicles_items_filter && 
				window._vehicles_items_filter_length>0 && 
				(!_vehicles_items_filter[_veh_id] || !_vehicles_items_filter[_veh_id+""])
			){
				continue;
			}
			var _vehicle_info = _vehicles_set[_veh_id];
			_str +=`
				<tr tr-id='${_items[idx].id}' role='row' class='${_count%2==0 ? 'odd':'even'}'>
					<td class="details-control sorting_1"></td> 
					<td>${_vehicle_info.plate}</td>
					<td>${_items[idx].price}</td>
					<td>
						<div reg-id='${_items[idx].id}' class="btn btn-default geo-api-btn-del item-del">
							<i class="fa fa-trash"></i>
						</div>
					</td> 
				</tr>
				<tr tr-ref='${_items[idx].id}' style='display:none'>
					<td colspan='5'>
						<div style='display:inline-block;text-align: left;width: 45%;vertical-align:top;'>
							<ul>
								<li>Marca: ${_vehicle_info.brand}</li>
								<li>Modelo: ${_vehicle_info.model}</li>
								<li>Placa: ${_vehicle_info.plate}</li>
								<li>IMEI: ${_vehicle_info.imei}</li>
								<li>SimCard: ${_vehicle_info.simcard}</li>
							</ul>
						</div>
						<div style='display:inline-block;text-align: left;width: 45%;vertical-align:top;'>
							<ul> 
								<li>Teléfono: ${_vehicle_info.phone}</li>
								<li>Protocolo: ${_vehicle_info.protocol}</li>
								<li>Nombre: ${_vehicle_info.name}</li>
								<li>Grupo: ${_vehicle_info.group}</li>
							</ul>
						</div>
					</td> 
				</tr>
			`;
			_count++;
		}
		items_table.html(_str);
		itemsEvents();
	}else{
		items_table.html("<tr><td colspan='4'></td></tr>");
	}
}
function itemsEvents(){
	$("#items_table>tbody>tr[role=row]>td:nth-child(1)").click(function(evt){
		var _this = $(this);
		if(_this.parent().hasClass("details")){
			_this.parent().removeClass("details");
			_this.parent().
			parent().find(`tr[tr-ref=${_this.parent().attr("tr-id")}]`).hide();
		}else{
			_this.parent().addClass("details");
			_this.parent().
			parent().find(`tr[tr-ref=${_this.parent().attr("tr-id")}]`).show();
		}
	});
	$(".item-del").click(function(evt){
		var _obj = $(this); 
		var _id = _obj.attr("reg-id");
		swal({
            title: "Confirmar la eliminación del registro ?",
            text: "Se eliminará este item en la factura.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Proceder",
            cancelButtonText: "Rechazar",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm){
            console.log("isConfirm",isConfirm); 
            if(isConfirm){  
            	var _data = {
            		session: sessionStorage.getItem('session'),
            		id:_id,
            	};  
            	console.log("beforeUpdate",_data);
				$.ajax({ 
			        url: _url_delete_item,
			        type: 'POST',
			        //type: 'PUT',
			        contentType: "application/json", 
			        crossDomain: true, 
			        data:JSON.stringify(_data),
	        		dataType:'json',
			        success: function(res, status ,xhr){
			        	console.log(res,status,xhr);
			        	if(status=="success"){
			        		setTimeout(function(){toastr_success_top_right("Registro eliminado", 'Actualización')}, 600);
			        	}else{
			        		setTimeout(function(){toastr_danger_top_right("Solicitud no procesada", 'Información')}, 600);
			        	}
	        			$(`#items_table tr[tr-id=${_id}]`).remove();
	        			$(`#items_table tr[tr-ref=${_id}]`).remove();
			        },
			        error: function (responseData, textStatus, errorThrown) {
			        	console.log(responseData,textStatus,errorThrown);
						setTimeout(function(){toastr_danger_top_right("Error al eliminar los cambios ", 'Error')}, 600);
						if(responseData.status == 401){
							toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
							updateSession(); 
						}
			        },
			        complete: function() { 
			        }
			    }); 
            }
        });   
	});
}
function is_numeric(num){
	if(typeof num=="object")return false;
	if(
		typeof num =="string" || 
		typeof num =="number" 
	){
		return /^\d+$/.test(""+num);
	}
	return false;
	
}