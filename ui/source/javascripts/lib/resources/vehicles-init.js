var modal =  $('#modalDashboard'),
    modalBody  = $('#modalDashboard .modal-body');
$(window).ready(function(evt){
	window._table = $("#vehicles_table"); 
	window._filter_order = 0;
	filter_list();  
	header_events();
	modal_events();
});
function header_events(){
	$("table.geo-api thead th:not(.disabled)").each(function(idx,obj){
		$(obj).addClass("fa geo-api-sort").click(function(evt){
			evt.preventDefault();
			if(window.prevent_filter_ajax)return;
			var _th_ref = this;
			var _ref = $(_th_ref);
			_table.attr("filter-key",_ref.attr("attr-key")); 
			if(_ref.hasClass("geo-api-sort")){
				_ref.removeClass("geo-api-sort").addClass("geo-api-sort-up");
				_filter_order = 1;
			}else if(_ref.hasClass("geo-api-sort-up")){
				_ref.removeClass("geo-api-sort-up").addClass("geo-api-sort-down");
				_filter_order = -1;
			}else{
				_ref.removeClass("geo-api-sort-down").addClass("geo-api-sort-up");
				_filter_order = 1;
			}
			$("table.geo-api thead th:not(.disabled)").each(function(ev){
				var _th = this;
				if(_th!=_th_ref){
					$(_th).removeClass("geo-api-sort-up geo-api-sort-down").addClass("geo-api-sort");
				}
			});
			filter_list();
		});
	});
	$("input[name=global_search]").keyup(function(evt){
		filter_list();
	});
}
function filter_list(){
	if(window.prevent_filter_ajax)return;
	window.prevent_filter_ajax = true;
	var _page = window._page_ref || "";
	var _data = {
		global_search:$("input[name=global_search]").val(),
		filter_order:_filter_order,
		filter_field:_table.attr("filter-key"),
		session: sessionStorage.getItem('session'),
		page:_page,
	}; 


	$.ajax({ 
        url: HOST_NAME+'/allVehicles',
        type: 'POST', 
        dataType:'json',
	 	contentType: "application/json", 
        data:JSON.stringify(_data), 
        success: function(response, status ,xhr){
        	if(window._veh_set)delete window._veh_set;
			window._veh_set = {};
			var _body = window._table.find("tbody").empty();
	 		var _str = "";
			if(response && response.data){
				window._clients = response.clients;
				var data = response.data;
				for(var idx in data){
					_veh_set[data[idx].id] = data[idx];
					_str +=
					`<tr>
						<td>${data[idx].id}</td>
						<td>${data[idx].plate}</td>
						<td>${data[idx].imei}</td>
						<td>${data[idx].brand}</td>
						<td>${data[idx].model}</td>
						<td>
							<div>
								<div reg-id="${data[idx].id}" class="btn btn-default geo-api-btn-del">
									<i class="fa fa-trash"></i>
								</div>
								<div reg-id="${data[idx].id}" class="btn btn-default geo-api-btn-edit">
									<i class="fa fa-edit"></i>
								</div>
							</div>
						</td>
					</tr>`;
				}
				_body.html(_str);
				$("#pag_table").html(getPagination(response.details));
				pagination_events();
				action_events();
			}else{
				_str =
				`<tr>
					<td colspan='5'>Registros no encontrados.</td> 
				</tr>`;
				_body.html(_str);
				$("#pag_table").html("");
			}
			console.log(response, status ,xhr);
			window.prevent_filter_ajax = false;
        },
        error: function (responseData, textStatus, errorThrown) {
        	if(window._veh_set)delete window._veh_set; 
			console.log(responseData, textStatus, errorThrown);
			window.prevent_filter_ajax = false;
			if(responseData.status == 401){
				toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
				sessionStorage.setItem("refreshPage", true);
                updateSession(); 
            }
        },
        complete: function() {  
        }
    }); 

	//$.get( HOST_NAME+'/allVehicles/'+_page,_data,function(response){},"json")
	/*$.get( HOST_NAME+'/allVehicles',_data,function(response){},"json")
	.done(function(response) {  
		if(window._veh_set)delete window._veh_set;
		window._veh_set = {};
		var _body = window._table.find("tbody").empty();
 		var _str = "";
		if(response && response.data){
			window._clients = response.clients;
			var data = response.data;
			for(var idx in data){
				_veh_set[data[idx].id] = data[idx];
				_str +=
				`<tr>
					<td>${data[idx].id}</td>
					<td>${data[idx].plate}</td>
					<td>${data[idx].imei}</td>
					<td>${data[idx].brand}</td>
					<td>${data[idx].model}</td>
					<td>
						<div>
							<div reg-id="${data[idx].id}" class="btn btn-default geo-api-btn-del">
								<i class="fa fa-trash"></i>
							</div>
							<div reg-id="${data[idx].id}" class="btn btn-default geo-api-btn-edit">
								<i class="fa fa-edit"></i>
							</div>
						</div>
					</td>
				</tr>`;
			}
			_body.html(_str);
			$("#pag_table").html(getPagination(response.details));
			pagination_events();
			action_events();
		}else{
			_str =
			`<tr>
				<td colspan='5'>Registros no encontrados.</td> 
			</tr>`;
			_body.html(_str);
			$("#pag_table").html("");
		}
		console.log( "second success",response );
		window.prevent_filter_ajax = false;
	})
	.fail(function(evt) { 
		if(window._veh_set)delete window._veh_set; 
		console.log( "error",evt );
		window.prevent_filter_ajax = false;
	}); */
}
function pagination_events(){
	var _nav = $("#nav-pag-table");
	_nav.find("li[page-ref]:not(.disabled)").click(function(evt){
		evt.preventDefault();
		var _li = this;
		window._page_ref = $(_li).attr("page-ref");
		filter_list();
	});
}
function action_events(){
	$(".geo-api-btn-del").click(function(evt){
		var _id = $(this).attr("reg-id");
		var _modal = $("#modal-veh-remove");
		_modal.find("[reg-id]").attr("reg-id",_id);
		_modal.modal("show");
	});
	$(".geo-api-btn-edit").click(function(evt){
		var _id = $(this).attr("reg-id");
		var _data = _veh_set[_id];
		var _modal = $("#modal-veh-details");
		_modal.attr("reg-id",_id);
		for(var _idx in _data){
			_modal.find(`[name=${_idx}]`).val(_data[_idx]);
		} 
		$("#client_info_name").html(_clients[_data.client_id].business_name);
		_modal.modal("show");
	});
}
function modal_events(){
	$("#del-reg").click(function(evt){
		var _id = $(this).attr("reg-id");
		var _data = {
	        	id:_id,
	        	session: sessionStorage.getItem('session'),
	        }; 
		$.ajax({
	        //url: HOST_NAME+'/delVehicle/'+_id,
	        url: HOST_NAME+'/delVehicle',
	        type: 'POST',
	        //type: 'DELETE',
	        dataType:'json',
	        contentType:'application/json',
	        data:JSON.stringify(_data), 
	        success: function(res, status ,xhr){
	        	console.log(res,status,xhr);
	        	if(status=="success"){
	        		setTimeout(function(){toastr_success_top_right("Registro eliminado", 'Eliminación')}, 600);
	        	}else{
	        		setTimeout(function(){toastr_danger_top_right("No se pudo eliminar el registro seleccionado.", 'Notificación')}, 600);
	        	}
	        	filter_list();
	        },
	        error: function (responseData, textStatus, errorThrown) {
	        	console.log(responseData,textStatus,errorThrown);
						 setTimeout(function(){toastr_danger_top_right("Error al intentar eliminar el registro.", 'Error')}, 600);
				if(responseData.status == 401){
					toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
					updateSession(); 
				}
	        },
	        complete: function() { 
	        	var _modal = $("#modal-veh-remove");
	        	_modal.modal("hide");
	        }
	    }); 
	});
	$("#btn-update").click(function(evt){
		swal({
            title: "Confirmar actualización ?",
            text: "Los cambios serán guardados en el servidor.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Proceder",
            cancelButtonText: "Rechazar",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm){
            console.log("isConfirm",isConfirm); 
            if(isConfirm){
            	var _modal = $("#modal-veh-details"); 
            	var _data = {
            		session: sessionStorage.getItem('session'),
            		id:_modal.attr("reg-id"),
            	}; 
            	_modal.find(".modal-body input").each(function(idx,ipt){
            		_data[$(ipt).attr("name")] = $(ipt).val();
            	});
            	console.log("beforeUpdate",_data);
				$.ajax({
			        //url: HOST_NAME+'/updateVehicle/'+_modal.attr("reg-id"),
			        url: HOST_NAME+'/updateVehicle',
			        type: 'POST',
			        //type: 'PUT',
			        contentType: "application/json", 
			        crossDomain: true, 
			        data:JSON.stringify(_data),
	        		dataType:'json',
			        success: function(res, status ,xhr){
			        	console.log(res,status,xhr);
			        	if(status=="success"){
			        		setTimeout(function(){toastr_success_top_right("Registro actualizado", 'Actualización')}, 600);
			        	}else{
			        		setTimeout(function(){toastr_danger_top_right("Cambios no actualizados", 'Actualización')}, 600);
			        	}
	        			filter_list();
			        },
			        error: function (responseData, textStatus, errorThrown) {
			        	console.log(responseData,textStatus,errorThrown);
						 setTimeout(function(){toastr_danger_top_right("Error al registrar los cambios ", 'Error')}, 600);
						 if(responseData.status == 401){
							toastr_info_top_right("Su sesión expiró, porfavor ingrese sus datos", 'Autorización');
							updateSession(); 
						}
			        },
			        complete: function() { 
			        }
			    }); 
            }
        });
	});  
}