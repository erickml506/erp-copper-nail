function getPagination(details){
	var li = ""; 
	for (var i = 1 ; i <= details.total_pages; i++) {
		if(i==details.current_page){
			li += ` 
		    	<li class="page-item active">
			      	<span class="page-link">
			        	${i}
			        	<span class="sr-only">(current)</span>
			      	</span>
		    	</li>
			`;
		}else{
			li +=`<li page-ref="${i}" class="page-item"><a class="page-link" href="#">${i}</a></li>`;
		}
	}
	return `
		<nav aria-label="vehicle-pagination" id="nav-pag-table">
		  	<ul class="pagination">
		    	<li page-ref="${(details.current_page-1)}" class="page-item ${details.current_page>1 ? "":"disabled"}">
		      		<span class="page-link">Anterior</span>
		    	</li>
		    	${li}
		    	<li page-ref="${(details.current_page+1)}" class="page-item ${details.current_page>=details.total_pages ? "disabled":""}">
		      		<a class="page-link" href="#">Siguiente</a>
		    	</li>
		  	</ul>
		</nav>
	`;
}
function getDocType(val){
	switch(val){
		case 0:
			val = "Consolidada";
		break;
		case 1:
			val = "Disgregada";
		break;
		default:
			val = "No Definido";
	}
	return val;
}

function getType(val){
	switch(val){
		case 0:
			val = "Boleta";
		break;
		case 1:
			val = "Factura";
		break;
		case 2:
			val = "Recibo";
		break;
		default:
			val = "No definido";
	}
	return val;
}
function getCycle(val){
	switch(val){
		case 20:
			val = "Reconexión u otros";
		break;
		case 0:
			val = "Sólo Dispositivo";
		break;
		case 1:
			val = "Mensual";
		break;
		case 3:
			val = "Trimestral";
		break;
		case 6:
			val = "Semestral";
		break;
		case 12:
			val = "Anual";
		break;
		default:
			val = "No definido";
	}
	return val;
}

