function createTemplate(){
    return `<form id="create_client">
                <div class="form-row">
                    <ul class="nav nav-tabs" role="tablist" style="width:100%;">
                        <li class="nav-item">
                            <a class="nav-link active" href="#primary" role="tab" data-toggle="tab">Datos principales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#secondary" role="tab" data-toggle="tab">Datos secundarios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#aditional" role="tab" data-toggle="tab">Datos adicionales</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="width:100%;margin-top: 15px;">
                        <div id="primary" class="tab-pane active" role="tabpanel">
                            <div class="form-group col-md-12">
                                <label for="type">Tipo</label>
                                <select class="form-control" name="type">
                                    <option value="Company">Compañía</option>
                                    <option value="Private">Propio</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="document">Documento <span id="documentError">Documento no válido</span></label>
                                <input class="form-control" type="text" name="document">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="business_name">Nombre <span id="nameError">Nombre inválido</span></label>
                                <input class="form-control" type="text" name="business_name">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="user">Usuario <span id="userError">Usuario inválido</span></label>
                                <input class="form-control" type="text" name="user">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Contraseña <span id="passError">Contraseña no válida</span></label>
                                <input class="form-control" type="password" name="password">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="service_type">Tipo de Servicio</label>
                                <select name="service_type" class="form-control">
                                    <option value="Anual">Anual</option>
                                    <option value="Trimestral">Trimestral</option>
                                    <option value="Mensual">Mensual</option>
                                </select>
                            </div>
                        </div>
                        <div id="secondary" class="tab-pane" role="tabpanel">
                            <div class="form-group col-md-12">
                                <label for="fiscal_address">Dirección fiscal <span id="fiscalAddressError">Dirección fiscal no válida</span></label>
                                <input class="form-control" type="text" name="fiscal_address">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="install_address">Dirección de instalación <span id="installAddressError">Dirección de instalación no válida</span></label>
                                <input class="form-control" type="text" name="install_address">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="install_hours">Horario de instalación <span id="installHoursError">Horario de instalación no válida</span></label>
                                <input class="form-control" type="text" name="install_hours">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="number_vehicles">Número de vehículos <span id="numbervError">Número de vehículos no válido</span></label>
                                <input class="form-control" type="number" min="1" name="number_vehicles">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="reference">Numero de Operacion <span id="referenceError">Referencia no válida</span></label>
                                <input class="form-control" type="text" name="operation_number">
                            </div>
                        </div>
                        <div id="aditional" class="tab-pane" role="tabpanel">
                            <div class="form-group col-md-12">
                                <label for="price_gps">Precio GPS <span id="priceGpsError">Precio GPS no válido</span></label>
                                <input class="form-control" type="number" step="0.01" min="0" name="price_gps">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="price_plataform">Precio Plataforma <span id="pricePlatError">Precio Plataforma no válida</span></label>
                                <input class="form-control" type="number" step="0.01" min="0" name="price_plataform">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="rent_expiration">Vencimiento de alquiler <span id="rentExpiPlatError">Vencimiento de alquiler no válido</span></label>
                                <input class="form-control" type="date" name="rent_expiration">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="purchase_order">Orden de compra <span id="purchaseOrderError">Orden de compra no válida</span></label>
                                <input class="form-control" type="text" name="purchase_order">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="payment_method">Método de pago</label>
                                <select name="payment_method" class="form-control">
                                    <option value="Cash">Efectivo</option>
                                    <option value="Visa">Visa</option>
                                    <option value="Down payment">Depósito</option>
                                    <option value="Transfer">Transferencia</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input"  type="checkbox"   name="is_rented">
                                    <label class="form-check-label" for="is_rented">
                                        Rented
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
                <button id="btn_create_client" type="submit" class="btn btn-primary">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}
function editTemplate(dataClient){
    //CAN BE BETTER
    var type, pmethod, isRented; 
    if(dataClient.type == 'Company'){
        type = `<option value="Company">Compañía</option>
                <option value="Private">Personal</option>`;
    }else{
        type = `<option value="Private">Personal</option>
                <option value="Company">Compañía</option>`
    }
    if(dataClient.payment_method == 'Cash'){
        pmethod = ` <option value="Cash">Efectivo</option>
                    <option value="Visa">Visa</option>
                    <option value="Down payment">Depósito</option>
                    <option value="Transfer">Transferencia</option>`;
    }else if(dataClient.payment_method == 'Visa'){
        pmethod = ` <option value="Visa">Visa</option>
                    <option value="Cash">Efectivo</option>
                    <option value="Down payment">Depósito</option>
                    <option value="Transfer">Transferencia</option>`;
    }else if(dataClient.payment_method == 'Down payment'){
        pmethod = ` <option value="Down payment">Depósito</option>
                    <option value="Visa">Visa</option>
                    <option value="Cash">Efectivo</option>
                    <option value="Transfer">Transferencia</option>`;
    }else if(dataClient.payment_method == 'Transfer'){
        pmethod = ` <option value="Transfer">Transferencia</option>
                    <option value="Down payment">Depósito</option>
                    <option value="Visa">Visa</option>
                    <option value="Cash">Efectivo</option>`;
    }
    if(dataClient.is_rented == "Si"){
        isRented = `<input class="form-check-input"  type="checkbox"   name="is_rented" checked>`;
    }else{
        isRented = `<input class="form-check-input"  type="checkbox"   name="is_rented">`;
    }
    var date = dataClient.rent_expiration;
    var convertDate = function(date) {
        var arrDate = date.split("-");
        return  arrDate.reverse().join('-');
    };
    return `<form id="edit_client">
                <div class="form-row">
                    <ul class="nav nav-tabs" role="tablist" style="width:100%;">
                        <li class="nav-item">
                            <a class="nav-link active" href="#primary" role="tab" data-toggle="tab">Datos principales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#secondary" role="tab" data-toggle="tab">Datos secundarios</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#aditional" role="tab" data-toggle="tab">Datos adicionales</a>
                        </li>
                    </ul>
                    <div class="tab-content" style="width:100%;margin-top: 15px;">
                        <div id="primary" class="tab-pane active" role="tabpanel">
                            <input type="hidden" name="id" value="${dataClient.id}">
                            <div class="form-group col-md-12">
                                <label for="type">Tipo</label>
                                <select class="form-control" name="type">
                                    ${type}
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="document">Documento <span id="documentError">Documento no válido</span></label>
                                <input class="form-control" type="text" name="document" value="${dataClient.document}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="business_name">Nombre <span id="nameError">Nombre no válido</span></label>
                                <input class="form-control" type="text" name="business_name" value="${dataClient.business_name}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="user">Usuario <span id="userError">Usuario no válido</span></label>
                                <input class="form-control" type="text" name="user" value="${dataClient.user}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Clave <span id="passError">Clave no válida</span></label>
                                <input class="form-control" type="text" name="password" value="${dataClient.password}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="service_type">Tipo de Servicio</label>
                                <select name="service_type" class="form-control">
                                    <option value="Anual" ${dataClient.service_type == 'Anual' ? 'selected' : ''}>Anual</option>
                                    <option value="Semestral" ${dataClient.service_type == 'Semestral' ? 'selected' : ''}>Semestral</option>
                                    <option value="Mensual" ${dataClient.service_type == 'Mensual' ? 'selected' : ''}>Mensual</option>
                                </select>
                            </div>
                        </div>
                        <div id="secondary" class="tab-pane" role="tabpanel">
                            <div class="form-group col-md-12">
                                <label for="fiscal_address">Dirección fiscal <span id="fiscalAddressError">Dirección fiscal no válida</span></label>
                                <input class="form-control" type="text" name="fiscal_address" value="${dataClient.fiscal_address}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="install_address">Dirección <span id="installAddressError">Dirección no válida</span></label>
                                <input class="form-control" type="text" name="install_address" value="${dataClient.install_address}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="install_hours">Horario de instalación <span id="installHoursError">Horario de instalación no válida</span></label>
                                <input class="form-control" type="text" name="install_hours" value="${dataClient.install_hours}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="number_vehicles">Número de véhículos <span id="numbervError">Número de vehículos no válido</span></label>
                                <input class="form-control" type="number" min="0" name="number_vehicles" value="${dataClient.number_vehicles}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="reference">Número de Operación <span id="referenceError">Referencia no válida</span></label>
                                <input class="form-control" type="text" name="operation_number" value="${dataClient.operation_number}">
                            </div>
                        </div>
                        <div id="aditional" class="tab-pane" role="tabpanel">
                            <div class="form-group col-md-12">
                                <label for="price_gps">Precio GPS <span id="priceGpsError">Precio GPS no válido</span></label>
                                <input class="form-control" type="number" step="0.01" min="0" name="price_gps" value="${dataClient.price_gps}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="price_plataform">Precio Plataforma <span id="pricePlatError">Precio Plataforma no válido</span></label>
                                <input class="form-control" type="number" step="0.01" min="0" name="price_plataform" value="${dataClient.price_plataform}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="rent_expiration">Vencimiento de alquiler <span id="rentExpiPlatError">Vencimiento de alquiler no válido</span></label>
                                <input class="form-control" type="date" name="rent_expiration" value="${convertDate(date)}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="purchase_order">Orden de compra <span id="purchaseOrderError">Orden de compra no válido</span></label>
                                <input class="form-control" type="text" name="purchase_order" value="${dataClient.purchase_order}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="payment_method">Método de pago</label>
                                <select name="payment_method" class="form-control">
                                    ${pmethod}
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                            <div class="form-check">
                                ${isRented}
                                <label class="form-check-label" for="is_rented">
                                    Rented
                                </label>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>    
                <button id="btn_edit_client" type="submit" class="btn btn-primary">Modificar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}
function addContactTemplate(idClient){
    return `<form id="form_create_contact">
                <div class="form-group">
                    <label for="name">Nombre <span id="nameError">Nombre no válido</span></label>
                    <input class="form-control" type="text" name="name">
                </div>
                <input type="hidden" name="id_client" value="${idClient}">
                <div class="form-group">
                    <label for="phone">Teléfono <span id="phoneError">Teléfono no válido</span></label>
                    <input class="form-control" type="text" name="phone">
                </div>
                <div class="form-group">
                    <label for="email">Email <span id="emailError">Email no válido</span></label>
                    <input class="form-control" type="email" name="email">
                </div>
                <div class="form-group">
                    <label for="relationship">Relación <span id="relError">Relación no válida</span></label>
                    <input class="form-control" type="text" name="relationship">
                </div>
                <div class="form-group">
                    <label for="type">Tipo</label>
                    <select class="form-control" name="type">
                        <option value="Owner">Propietario</option>
                        <option value="Billing">Factura</option>
                        <option value="Support">Soporte</option>
                    </select>
                </div>
                <button id="btn_create_contact" type="submit" class="btn btn-primary">Agregar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}
function showContactTemplate(){
    return `<div class="table-responsive">
                <table class="table table-hover tableContacts">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Teléfono</th>
                            <th>Relación</th>
                            <th>Email</th>
                            <th>Tipo</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                
                    </tbody>
                </table>
            </div>`;
}
function showContactRowTemplate(res, i, idClient){
    return `<tr>
                <td>${res[i].name}</td>
                <td>${res[i].phone}</td>
                <td>${res[i].relationship}</td>
                <td>${res[i].email}</td>
                <td>${res[i].type}</td>
                <td style="display:none">${idClient}</td>
                <td>
                    <button id="edit_contact" data-id="${res[i].id}" class="btn btn-info" title="Editar contacto" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-pencil-square-o"></i></button>
                    <button id="delete_contact" data-id="${res[i].id}" class="btn btn-danger" title="Eliminar contacto" style="width: 38px;height: 40px;padding: 0;"><i class="fa fa-trash"></i></button>
                </td>
            </tr>`
}
function editContactTemplate(dataRow, idContact, idClient){
    let type;
    if(dataRow[4] == 'Owner'){
        type = `<option value="Owner">Propietario</option>
                <option value="Billing">Billing</option>
                <option value="Support">Soporte</option>`;
    }else if(dataRow[4] == 'Billing'){
        type = `<option value="Billing">Billing</option>
                <option value="Owner">Propietario</option>
                <option value="Support">Soporte</option>`;
    }else if(dataRow[4] == 'Support'){
        type = `<option value="Support">Soporte</option>
                <option value="Billing">Billing</option>
                <option value="Owner">Propietario</option>`;
    }
    return ` <form id="form_edit_contact">
                <div class="form-group">
                    <label for="name">Nombre <span id="nameError">Nombre no válido</span></label>
                    <input class="form-control" type="text" name="name" value="${dataRow[0]}">
                </div>
                <input type="hidden" name="id_client" value="${idClient}">
                <input type="hidden" name="id_contact" value="${idContact}">
                <div class="form-group">
                    <label for="phone">Teléfono <span id="phoneError">Teléfono no válido</span></label>
                    <input class="form-control" type="text" name="phone" value="${dataRow[1]}">
                </div>
                <div class="form-group">
                    <label for="email">Email <span id="emailError">Email no válido</span></label>
                    <input class="form-control" type="email" name="email" value="${dataRow[3]}">
                </div>
                <div class="form-group">
                    <label for="relationship">Relación <span id="relError">Relación no válida</span></label>
                    <input class="form-control" type="text" name="relationship" value="${dataRow[2]}">
                </div>
                <div class="form-group">
                    <label for="type">Tipo</label>
                    <select class="form-control" name="type">
                        ${type}
                    </select>
                </div>
                <button id="btn_edit_contact" type="submit" class="btn btn-primary">Modifiar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}

function showVehicleTemplate(){
    return `<div class="table-responsive">
                <table id="vehicles_table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Más</th>
                            <th>Imei</th>
                            <th>Nombre</th>
                            <th>grupo</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Más</th>
                            <th>Imei</th>
                            <th>Nombre</th>
                            <th>Grupo</th>
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                </table>
            </div>`;
}

function editVehicleTemplate(dataVehicle, res){
    const groups = function(agreements){
        let options = '';
        agreements.forEach(function(agreement) {
            if(agreement.vehicle_group == dataVehicle.group){
                options += '<option value="'+agreement.vehicle_group+'" selected>'+agreement.vehicle_group+'</option>'
            }else options += '<option value="'+agreement.vehicle_group+'">'+agreement.vehicle_group+'</option>'  
        });
        return options;
    }
	return `
		<form id="form_edit_vehicle">
			<div class="form-row">
				<ul class="nav nav-tabs" role="tablist" style="width:100%;">
					<li class="nav-item">
						<a class="nav-link active" href="#primary" role="tab" data-toggle="tab">Datos principales</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#secondary" role="tab" data-toggle="tab">Datos secundarios</a>
					</li>
				</ul>
				<div class="tab-content" style="width:100%;margin-top: 15px;">
					<div id="primary" class="tab-pane active" role="tabpanel">
						<div class="form-group col-md-12">
							<label for="imei">IMEI</label>
							<input class="form-control" name="imei" type="text" value="${dataVehicle.imei}">
						</div>
                        <input type="hidden" name="client_id" value="${dataVehicle.client_id}">
                        <input type="hidden" name="id" value="${dataVehicle.id}">
						<div class="form-group col-md-12">
							<label for="name">Nombre</label>
							<div>
								<input class="form-control" name="name" type="text" value="${dataVehicle.name}">
							</div>
						</div> 
						<div class="form-group col-md-12">
							<label for="brand">Marca</label>
							<input class="form-control" name="brand" type="text" value="${dataVehicle.brand}">
                        </div>
                        <div class="form-group col-md-12">
							<label for="model">Grupo</label>
                            <select class="form-control" name="group">
                                <option value="">Selecciona un Grupo</option>
                                ${groups(res.data)}
                            </select>
						</div> 
					</div>
					<div id="secondary" class="tab-pane" role="tabpanel"> 
                       <div class="form-group col-md-12">
							<label for="model">Modelo</label>
							<input class="form-control" name="model" type="text" value="${dataVehicle.model}">
						</div> 
						<div class="form-group col-md-12">
							<label for="plate">Placa</label>
							<input class="form-control" name="plate" type="text" value="${dataVehicle.plate}">
						</div> 
						<div class="form-group col-md-12">
							<label for="phone">Teléfono</label>
							<input class="form-control" name="phone" type="text" value="${dataVehicle.phone}">
						</div> 
						<div class="form-group col-md-12">
							<label for="simcard">SimCard</label>
							<input class="form-control" name="simcard" type="text" value="${dataVehicle.simcard}">
						</div> 
						<div class="form-group col-md-12">
							<label class="col-form-label">Protocolo</label>
							<input class="form-control" name="protocol" type="text" value="${dataVehicle.protocol}">
						</div> 
					</div>
			    </div>
            </div>
            <button id="btn_edit_vehicle" type="submit" class="btn btn-primary">Editar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		</form>`;

}

function addAgreementTemplate(idClient){
    return ` <form id="form_create_agreement">
                <div class="form-group">
                    <label form="vehicle_group">Grupo de vehiculo<span id="groupError">Grupo no válido</span></label>
                    <input class="form-control" type="text" name="vehicle_group">
                </div>
                <input type="hidden" name="client_id" value=${idClient}>
                <div class="form-group">
                    <label form="start_date">Fehca de inicio <span id="startDateError">Fecha no válida</span></label>
                    <input class="form-control" type="date" name="start_date">
                </div>
                <div class="form-group">
                    <label form="due_date">Fecha de vencimiento <span id="dueDateError">Fecha no válida</span></label>
                    <input class="form-control" type="date" name="due_date">
                </div>
     
                <button id="btn_create_agreement" type="submit" class="btn btn-primary">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}
function editAgreementTemplate(data){
    return ` <form id="form_edit_agreement">
                <div class="form-group">
                    <label form="vehicle_group">Grupo de vehiculo<span id="groupError">Grupo no válido</span></label>
                    <input class="form-control" type="text" name="vehicle_group" value="${data.vehicle_group}">
                </div>
                <input type="hidden" name="client_id" value="${data.client_id}">
                <input type="hidden" name="id" value="${data.id}">
                <div class="form-group">
                    <label form="start_date">Fehca de inicio <span id="startDateError">Fecha no válida</span></label>
                    <input class="form-control" type="date" name="start_date" value="${data.start_date}">
                </div>
                <div class="form-group">
                    <label form="due_date">Fecha de vencimiento <span id="dueDateError">Fecha no válida</span></label>
                    <input class="form-control" type="date" name="due_date" value="${data.due_date}">
                </div>
     
                <button id="btn_edit_agreement" type="submit" class="btn btn-primary">Editar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}
function showAgreementTemplate(){
    return `<div class="table-responsive">
                <table id="agreements_table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Grupo de Vehiculos</th>
                            <th>Fecha de inicio</th>
                            <th>Fecha de vencimiento</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Grupo de Vehiculos</th>
                            <th>Fecha de inicio</th>
                            <th>Fecha de vencimiento</th>
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                </table>
            </div>`;
}