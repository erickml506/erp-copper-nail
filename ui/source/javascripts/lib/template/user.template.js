function createTemplate(){
    return ` <form id="create_user">
                <div class="form-group">
                    <label form="name">Nombre <span id="nameError">Nombre no válido</span></label>
                    <input class="form-control" type="text" name="name">
                </div>
                <div class="form-group">
                    <label form="lastname">Apellido <span id="lastError">Apellido no válido</span></label>
                    <input class="form-control" type="text" name="apellidos">
                </div>
                <div class="form-group">
                    <label form="email">Email <span id="emailError">Email no válido</span></label>
                    <input class="form-control" type="email" name="email">
                </div>
                <div class="form-group">
                    <label form="password">Clave <span id="passError">Clave no válido</span></label>
                    <input class="form-control" type="password" name="password">
                </div>
                <div class="form-group">
                    <label form="repassword">Repetir clave <span id="repassError">La clave no coincide</span></label>
                    <input class="form-control" type="password" name="repassword">
                </div>
                <div class="form-group">
                    <label form="type">Nivel de permiso</label>
                    <select name="type" class="form-control">
                        <option value="user">Usuario</option>
                        <option value="admin">Administrador</option>
                    </select>
                </div>
                <button id="btn_create_user" type="submit" class="btn btn-primary">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}
function editTemplate(dataUser){
    let level;
    if(dataUser.type == 'admin'){
        level = `  <div class="form-group">
                        <label form="type">Nivel de permiso</label>
                        <select name="type" class="form-control">
                            <option value="admin">Administrador</option>
                            <option value="user">Usuario</option> 
                        </select>
                    </div>`;
    }else if(dataUser.type == 'user'){
        level = `   <div class="form-group">
                        <label form="type">Nivel de permiso</label>
                        <select name="type" class="form-control">
                            <option value="user">Usuario</option> 
                            <option value="admin">Administrador</option>  
                        </select>
                    </div>`;
    }
    return  ` <form id="edit_user">
                <input type="hidden" name="id" value="${dataUser.id}">
                <div class="form-group">
                    <label form="name">Nombre <span id="nameError">Nombre no válido</span></label>
                    <input class="form-control" type="text" name="name" value="${dataUser.name}">
                </div>
                <div class="form-group">
                    <label form="lastname">Apellido <span id="lastError">Apellido no válido</span></label>
                    <input class="form-control" type="text" name="apellidos" value="${dataUser.apellidos}">
                </div>
                <div class="form-group">
                    <label form="email">Email <span id="emailError">Email no válido</span></label>
                    <input class="form-control" type="email" name="email" value="${dataUser.email}">
                </div>
                <div class="form-group">
                    <label form="password">Clave <span id="passError">Clave no válida</span></label>
                    <input class="form-control" type="password" name="password">
                </div>
                <div class="form-group">
                    <label form="repassword">Repetir clave <span id="repassError">La clave no coincide</span></label>
                    <input class="form-control" type="password" name="repassword">
                </div>
                ${level}
                <button id="btn_edit_user" type="submit" class="btn btn-primary">Guardar cambios</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </form>`;
}