function getPagination(details){
	var li = ""; 
	for (var i = 1 ; i <= details.total_pages; i++) {
		if(i==details.current_page){
			li += ` 
		    	<li class="page-item active">
			      	<span class="page-link">
			        	${i}
			        	<span class="sr-only">(current)</span>
			      	</span>
		    	</li>
			`;
		}else{
			li +=`<li page-ref="${i}" class="page-item"><a class="page-link" href="#">${i}</a></li>`;
		}
	}
	return `
		<nav aria-label="vehicle-pagination" id="nav-pag-table">
		  	<ul class="pagination">
		    	<li page-ref="${(details.current_page-1)}" class="page-item ${details.current_page>1 ? "":"disabled"}">
		      		<span class="page-link">Anterior</span>
		    	</li>
		    	${li}
		    	<li page-ref="${(details.current_page+1)}" class="page-item ${details.current_page>=details.total_pages ? "disabled":""}">
		      		<a class="page-link" href="#">Siguiente</a>
		    	</li>
		  	</ul>
		</nav>
	`;
}

